import request from '../utils/request';

export function cartPage(params) {
  return request({
    url: '/cart/page',
    method: 'get',
    params: params
  })
}

export function cartAdd(data) {
  return request({
    url: '/cart/add',
    method: 'post',
    data: data,
  })
}

export function cartUpdate(data) {
  return request({
    url: '/cart/update',
    method: 'post',
    data: data,
  })
}

export function cartDelete(params) {
  return request({
    url: '/cart/delete',
    method: 'get',
    params: params
  })
}

  
