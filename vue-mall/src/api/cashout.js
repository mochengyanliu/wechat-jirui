import request from '../utils/request';

export function cashoutPage(params) {
  return request({
    url: '/cashout/page',
    method: 'get',
    params: params
  })
}

export function cashoutAdd(data) {
  return request({
    url: '/cashout/add',
    method: 'post',
    data: data,
  })
}

export function cashoutUpdate(data) {
  return request({
    url: '/cashout/update',
    method: 'post',
    data: data,
  })
}

export function cashoutDelete(params) {
  return request({
    url: '/cashout/delete',
    method: 'get',
    params: params
  })
}

  
