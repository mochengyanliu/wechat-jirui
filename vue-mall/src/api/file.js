import request from '../utils/request';

export function fileUpload(data) {
  return request({
    url: '/file/upload',
    method: 'post',
    data: data,
    timeout: 12*60*60*1000
  })
}


  
