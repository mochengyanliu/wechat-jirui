import request from '../utils/request';

export function goodsPage(params) {
  return request({
    url: '/goods/page',
    method: 'get',
    params: params
  })
}

export function goodsAdd(data) {
  return request({
    url: '/goods/add',
    method: 'post',
    data: data,
  })
}

export function goodsUpdate(data) {
  return request({
    url: '/goods/update',
    method: 'post',
    data: data,
  })
}

export function goodsDelete(params) {
  return request({
    url: '/goods/delete',
    method: 'get',
    params: params
  })
}

export function goodsDetail(params) {
  return request({
    url: '/goods/detail',
    method: 'get',
    params: params
  })
}
  
