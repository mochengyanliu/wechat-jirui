import request from '../utils/request';

export function categoryPage(params) {
  return request({
    url: '/category/page',
    method: 'get',
    params: params
  })
}

export function categoryAdd(data) {
  return request({
    url: '/category/add',
    method: 'post',
    data: data,
  })
}

export function categoryUpdate(data) {
  return request({
    url: '/category/update',
    method: 'post',
    data: data,
  })
}

export function categoryDelete(params) {
  return request({
    url: '/category/delete',
    method: 'get',
    params: params
  })
}

export function categoryList(params) {
  return request({
    url: '/category/list',
    method: 'get',
    params: params
  })
}

  
