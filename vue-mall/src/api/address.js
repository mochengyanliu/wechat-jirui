import request from '../utils/request';

export function addressPage(params) {
  return request({
    url: '/address/page',
    method: 'get',
    params: params
  })
}

export function addressAdd(data) {
  return request({
    url: '/address/add',
    method: 'post',
    data: data,
  })
}

export function addressUpdate(data) {
  return request({
    url: '/address/update',
    method: 'post',
    data: data,
  })
}

export function addressDelete(params) {
  return request({
    url: '/address/delete',
    method: 'get',
    params: params
  })
}

export function addressList(params) {
  return request({
    url: '/address/list',
    method: 'get',
    params: params
  })
}

export function addressByUser(params) {
  return request({
    url: '/address/selectByUserId',
    method: 'get',
    params: params
  })
}
  
