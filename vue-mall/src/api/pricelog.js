import request from '../utils/request';

export function pricelogPage(params) {
  return request({
    url: '/pricelog/page',
    method: 'get',
    params: params
  })
}

export function pricelogPageList(params) {
  return request({
    url: '/pricelog/pageList',
    method: 'get',
    params: params
  })
}

export function pricelogAdd(data) {
  return request({
    url: '/pricelog/add',
    method: 'post',
    data: data,
  })
}

export function pricelogUpdate(data) {
  return request({
    url: '/pricelog/update',
    method: 'post',
    data: data,
  })
}

export function pricelogDelete(params) {
  return request({
    url: '/pricelog/delete',
    method: 'get',
    params: params
  })
}

  
