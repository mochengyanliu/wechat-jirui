import request from '../utils/request';

export function brandPage(params) {
  return request({
    url: '/brand/page',
    method: 'get',
    params: params
  })
}

export function brandAdd(data) {
  return request({
    url: '/brand/add',
    method: 'post',
    data: data,
  })
}

export function brandUpdate(data) {
  return request({
    url: '/brand/update',
    method: 'post',
    data: data,
  })
}

export function brandDelete(params) {
  return request({
    url: '/brand/delete',
    method: 'get',
    params: params
  })
}

export function brandList(params) {
  return request({
    url: '/brand/list',
    method: 'get',
    params: params
  })
}

  
