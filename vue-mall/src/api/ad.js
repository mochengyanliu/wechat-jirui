import request from '../utils/request';

export function adPage(params) {
  return request({
    url: '/ad/page',
    method: 'get',
    params: params
  })
}

export function adAdd(data) {
  return request({
    url: '/ad/add',
    method: 'post',
    data: data,
  })
}

export function adUpdate(data) {
  return request({
    url: '/ad/update',
    method: 'post',
    data: data,
  })
}

export function adDelete(params) {
  return request({
    url: '/ad/delete',
    method: 'get',
    params: params
  })
}

export function adList(params) {
  return request({
    url: '/ad/list',
    method: 'get',
    params: params
  })
}

  
