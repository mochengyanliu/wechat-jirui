import request from '../utils/request';

export function orderPage(params) {
  return request({
    url: '/order/page',
    method: 'get',
    params: params
  })
}

export function orderAdd(data) {
  return request({
    url: '/order/add',
    method: 'post',
    data: data,
  })
}

export function orderUpdate(data) {
  return request({
    url: '/order/update',
    method: 'post',
    data: data,
  })
}

export function orderDelete(params) {
  return request({
    url: '/order/delete',
    method: 'get',
    params: params
  })
}

export function orderPayment(data) {
  return request({
    url: '/payment/createOrder',
    method: 'post',
    data: data,
  })
}

export function orderPay(params) {
  return request({
    url: '/payment/pay',
    method: 'get',
    params: params
  })
}

  
