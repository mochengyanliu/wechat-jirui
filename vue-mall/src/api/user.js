import request from '../utils/request';

export function userPage(params) {
  return request({
    url: '/user/page',
    method: 'get',
    params: params
  })
}

export function userList(params) {
  return request({
    url: '/user/list',
    method: 'get',
    params: params
  })
}

export function userAdd(data) {
  return request({
    url: '/user/add',
    method: 'post',
    data: data,
  })
}

export function userUpdate(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data: data,
  })
}

export function userDelete(params) {
  return request({
    url: '/user/delete',
    method: 'get',
    params: params
  })
}

export function userCheck(params) {
  return request({
    url: '/user/check',
    method: 'get',
    params: params
  })
}

export function userIsOpen(params) {
  return request({
    url: '/user/isOpen',
    method: 'get',
    params: params
  })
}

export function userUpdateOpen(data) {
  return request({
    url: '/user/updateOpen',
    method: 'post',
    data: data,
  })
}

export function countPage(params) {
  return request({
    url: '/user/countPage',
    method: 'get',
    params: params
  })
}



  
