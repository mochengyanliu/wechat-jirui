import Vue from 'vue'
//引入需要的组件
import { Button } from "vant";
import { NavBar } from 'vant';
import { Tabbar, TabbarItem } from 'vant';
import { Tab, Tabs } from 'vant';
import { Icon } from 'vant';
import { List } from 'vant';
import { Cell, CellGroup } from 'vant';
import { PullRefresh } from 'vant';
import { Panel } from 'vant';
import { Image as VanImage } from 'vant';
import { Step, Steps } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import { GoodsAction, GoodsActionIcon, GoodsActionButton } from 'vant';
import { AddressList } from 'vant';
import { Stepper } from 'vant';

//一定要注册组件
Vue.use(NavBar);
Vue.use(Button);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Icon);
Vue.use(List);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(PullRefresh);
Vue.use(Panel);
Vue.use(VanImage);
Vue.use(Step);
Vue.use(Steps);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(GoodsAction);
Vue.use(GoodsActionButton);
Vue.use(GoodsActionIcon);
Vue.use(AddressList);
Vue.use(GoodsAction);
Vue.use(GoodsActionButton);
Vue.use(GoodsActionIcon);
Vue.use(Stepper);









