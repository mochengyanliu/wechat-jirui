import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/dashboard',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Dashboard.vue'),
                    meta: { title: '系统首页' }
                },
                {
                    path: '/usertable',
                    component: () => import(/* webpackChunkName: "usertable" */ '../components/page/UserTable.vue'),
                    meta: { title: '用户管理' }
                },
                {
                    path: '/categorytable',
                    component: () => import(/* webpackChunkName: "icategorytablecon" */ '../components/page/CategoryTable.vue'),
                    meta: { title: '类目管理' }
                },
                {
                    path: '/brandtable',
                    component: () => import(/* webpackChunkName: "brandtable" */ '../components/page/BrandTable.vue'),
                    meta: { title: '品牌商管理' }
                },
                {
                    path: '/adtable',
                    component: () => import(/* webpackChunkName: "adtable" */ '../components/page/AdTable.vue'),
                    meta: { title: '广告管理' }
                },
                {
                    path: '/goodstable',
                    component: () => import(/* webpackChunkName: "goodstable" */ '../components/page/GoodsTable.vue'),
                    meta: { title: '商品管理' }
                },
                {
                    path: '/goodsadd',
                    name: 'goodsadd',
                    component: () => import(/* webpackChunkName: "goodsadd" */ '../components/page/GoodsAdd.vue'),
                    meta: { title: '维护商品' }
                },
                {
                    path: '/addresstable',
                    component: () => import(/* webpackChunkName: "addresstable" */ '../components/page/AddressTable.vue'),
                    meta: { title: '收货地址管理' }
                },
                {
                    path: '/ordertable',
                    component: () => import(/* webpackChunkName: "ordertable" */ '../components/page/OrderTable.vue'),
                    meta: { title: '订单管理' }
                },
                {
                    path: '/carttable',
                    component: () => import(/* webpackChunkName: "carttable" */ '../components/page/CartTable.vue'),
                    meta: { title: '购物车管理' }
                },
                {
                    path: '/pricelogtable',
                    component: () => import(/* webpackChunkName: "carttable" */ '../components/page/PricelogTable.vue'),
                    meta: { title: '消费记录管理' }
                },
                {
                    path: '/cashouttable',
                    component: () => import(/* webpackChunkName: "carttable" */ '../components/page/CashoutTable.vue'),
                    meta: { title: '提现管理' }
                },
                {
                    path: '/counttable',
                    component: () => import(/* webpackChunkName: "carttable" */ '../components/page/CountTable.vue'),
                    meta: { title: '统计管理' }
                },
                {
                    path: '/404',
                    component: () => import(/* webpackChunkName: "404" */ '../components/page/404.vue'),
                    meta: { title: '404' }
                },
                {
                    path: '/403',
                    component: () => import(/* webpackChunkName: "403" */ '../components/page/403.vue'),
                    meta: { title: '403' }
                }
            ]
        },
        
        {
            path: '/index',
            redirect: '/index/home'
        },
        {
            path: '/index',
            component: () => import(/* webpackChunkName: "home" */ '../components/page/phone'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/index/home',
                    component: () => import(/* webpackChunkName: "home" */ '../components/page/phone/home'),
                    meta: { title: '首页' }
                },
                {
                    path: '/index/mine',
                    component: () => import(/* webpackChunkName: "mine" */ '../components/page/phone/mine'),
                    meta: { title: '我的' }
                },
                {
                    path: '/index/detail',
                    component: () => import(/* webpackChunkName: "detail" */ '../components/page/phone/detail'),
                    meta: { title: '商品详情' }
                },
                {
                    path: '/index/buy',
                    component: () => import(/* webpackChunkName: "detail" */ '../components/page/phone/buy'),
                    meta: { title: '提交订单' }
                },
            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
