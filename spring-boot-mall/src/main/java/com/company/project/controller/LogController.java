package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Log;
import com.company.project.service.LogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "操作日志模块")
@RestController
@RequestMapping("/log")
public class LogController {
    @Resource
    private LogService logService;

    @ApiOperation("操作日志新增")
    @PostMapping("/add")
    public Result add(@RequestBody Log log) {
        logService.save(log);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("操作日志删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "操作日志ID", required = true) @RequestParam Integer id) {
        logService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("操作日志修改")
    @PostMapping("/update")
    public Result update(@RequestBody Log log) {
        logService.update(log);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("操作日志详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "操作日志ID", required = true) @RequestParam Integer id) {
        Log log = logService.findById(id);
        return ResultGenerator.genSuccessResult(log);
    }

    @ApiOperation("操作日志分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Log> list = logService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
