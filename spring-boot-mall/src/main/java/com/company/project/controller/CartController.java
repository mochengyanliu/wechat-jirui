package com.company.project.controller;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Cart;
import com.company.project.model.Order;
import com.company.project.service.CartService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Api(tags = "购物车商品模块")
@RestController
@RequestMapping("/cart")
public class CartController {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private CartService cartService;

    @ApiOperation("购物车商品新增")
    @PostMapping("/add")
    public Result add(@RequestBody Cart cart) {
        Cart old = cartService.selectByGoodsId(cart.getUserId(), cart.getGoodsId());
        if (old != null) {
            old.setCartNumber(old.getCartNumber() + cart.getCartNumber());
            cartService.update(old);
        } else {
            cartService.save(cart);
        }
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("购物车商品删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "购物车商品详情ID", required = true) @RequestParam List<Integer> ids) {
        for (Integer id : ids) {
            Cart cart = new Cart();
            cart.setId(id);
            cart.setStatus(0);
            cartService.update(cart);
        }
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("购物车商品修改")
    @PostMapping("/update")
    public Result update(@RequestBody Cart cart) {
        cartService.update(cart);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("购物车商品详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "购物车商品详情ID", required = true) @RequestParam Integer id) {
        Cart cart = cartService.findById(id);
        return ResultGenerator.genSuccessResult(cart);
    }

    @ApiOperation("购物车商品分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "商品名称", required = false) @RequestParam("goodsName") String goodsName,
                       @ApiParam(value = "购物车用户", required = false) @RequestParam("userId") Integer userId) {
        PageHelper.startPage(page, size);
        List<Cart> list = cartService.page(userId, goodsName);
        for (Cart cart : list) {
            if (StringUtils.isNotEmpty(cart.getPicUrl())) {
                cart.setPicUrlSrc(picurl + cart.getPicUrl());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
