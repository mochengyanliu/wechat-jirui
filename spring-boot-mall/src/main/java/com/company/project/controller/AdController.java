package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Ad;
import com.company.project.service.AdService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "广告模块")
@RestController
@RequestMapping("/ad")
public class AdController {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private AdService adService;

    @ApiOperation("广告新增")
    @PostMapping("/add")
    public Result add(@RequestBody Ad ad) {
        adService.save(ad);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("广告删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "广告ID", required = true) @RequestParam Integer id) {
        Ad ad = new Ad();
        ad.setId(id);
        ad.setStatus(0);
        adService.update(ad);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("广告修改")
    @PostMapping("/update")
    public Result update(@RequestBody Ad ad) {
        adService.update(ad);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("广告详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "广告ID", required = true) @RequestParam Integer id) {
        Ad ad = adService.findById(id);
        return ResultGenerator.genSuccessResult(ad);
    }

    @ApiOperation("广告分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "广告标题", required = true) @RequestParam("title") String title) {
        PageHelper.startPage(page, size);
        Condition condition = new Condition(Ad.class);
        condition.createCriteria().andEqualTo("status", 1).andLike("title", "%"+title+"%");
        List<Ad> list = adService.findByCondition(condition);
        for (Ad ad : list) {
            if (StringUtils.isNotEmpty(ad.getUrl())) {
                ad.setUrlSrc(picurl + ad.getUrl());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
