package com.company.project.controller;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.internal.util.codec.Base64;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Goods;
import com.company.project.model.Order;
import com.company.project.service.GoodsService;
import com.company.project.service.OrderService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Api(tags = "商品基本信息模块")
@RestController
@RequestMapping("/goods")
public class GoodsController {
    @Resource
    private GoodsService goodsService;
    @Resource
    private OrderService orderService;

    @Value("${pic.url}")
    private String picurl;

    @ApiOperation("商品基本信息新增")
    @PostMapping("/add")
    public Result add(@RequestBody Goods goods) {
        int begin = (int) (goods.getCounterPrice().divide(goods.getRetailPrice()).intValue() * 0.7);
        int end = goods.getCounterPrice().divide(goods.getRetailPrice()).intValue();
        int random = (int) (Math.random() * (end - begin));
        goods.setRandom(random + begin);
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= goods.getCounterPrice().divide(goods.getRetailPrice()).intValue(); i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        goods.setRandomList(StringUtils.join(list, ","));
        goodsService.save(goods);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("商品基本信息删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "商品基本信息ID", required = true) @RequestParam Integer id) {
        Goods goods = new Goods();
        goods.setId(id);
        goods.setStatus(0);
        goodsService.update(goods);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("商品基本信息修改")
    @PostMapping("/update")
    public Result update(@RequestBody Goods goods) {
        Goods good = goodsService.selectById(goods.getId());
        if (good.getCounterPrice().compareTo(goods.getCounterPrice()) != 0) {
            int begin = (int) (goods.getCounterPrice().divide(goods.getRetailPrice()).intValue() * 0.7);
            int end = goods.getCounterPrice().divide(goods.getCounterPrice()).intValue();
            int random = (int) (Math.random() * (end - begin));
            goods.setRandom(random + begin);
        }
        goodsService.update(goods);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("商品基本信息详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "商品基本信息ID", required = true) @RequestParam Integer id,
                         @ApiParam(value = "订单ID", required = true) @RequestParam Integer orderId) {
        Goods goods = goodsService.selectById(id);
        if (StringUtils.isNotEmpty(goods.getPicUrl())) {
            goods.setPicUrlSrc(picurl + goods.getPicUrl());
        }
        String[] gallery = goods.getGallery().split(",");
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        list1.add(goods.getPicUrl());
        list2.add(goods.getPicUrlSrc());
        for (String s : gallery) {
            list1.add(s);
            list2.add(picurl + s);
        }
        goods.setGallerys(list1);
        goods.setGalleryUrls(list2);
        goods.setDetail(goods.getDetail() == null ? "" : goods.getDetail());
        if (goods.getDifferPrice().compareTo(BigDecimal.ZERO) == 0) {
            Calendar date = Calendar.getInstance();
            int mi = date.get(Calendar.MINUTE);
            int se = date.get(Calendar.SECOND);
            int cu = 5;
            while (mi > cu) {
                cu += 5;
            }
            goods.setSecond(cu * 60 + 10 - mi * 60 - se);
        }

        // 换取记录
        if (orderId != null) {
            Order order = orderService.selectById(orderId);
            String date = order.getWinDate();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            order.setWinDate(format.format(order.getWinTime()));
            try {
                order.setWinTime(format.parse(date));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            goods.setOrder(order);

            List<Order> records = orderService.listByOrderIssue(order.getOrderIssue());
            goods.setRecords(records);
        } else {
            List<Order> records = orderService.listByGoodsId(id);
            goods.setRecords(records);
        }

        // 历史中奖
        List<Order> historys = orderService.historyByGoodsId(id);
        goods.setHistorys(historys);

        return ResultGenerator.genSuccessResult(goods);
    }

    @ApiOperation("商品基本信息分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "商品名", required = false) @RequestParam("goodsName") String goodsName,
                       @ApiParam(value = "是否上架", required = false) @RequestParam("isOnSale") Integer isOnSale,
                       @ApiParam(value = "是否可购买", required = false) @RequestParam("isBuy") Integer isBuy,
                       @ApiParam(value = "是否可金币兑换", required = false) @RequestParam("isGold") Integer isGold,
                       @ApiParam(value = "类目ID", required = false) @RequestParam("categoryId") Integer categoryId,
                       @ApiParam(value = "类型：know,user,time,price", required = false) @RequestParam("type") String type) {
        PageHelper.startPage(page, size);
        List<Goods> list = goodsService.selectPage(goodsName, categoryId, type, isOnSale, isBuy, isGold);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @ApiOperation("即将揭晓分页")
    @GetMapping("/knowPage")
    public Result knowPage(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                           @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                           @ApiParam(value = "商品名", required = false) @RequestParam("goodsName") String goodsName,
                           @ApiParam(value = "是否上架", required = false) @RequestParam("isOnSale") Integer isOnSale,
                           @ApiParam(value = "是否可购买", required = false) @RequestParam("isBuy") Integer isBuy,
                           @ApiParam(value = "类目ID", required = false) @RequestParam("categoryId") Integer categoryId,
                           @ApiParam(value = "类型：know,user,time,price", required = false) @RequestParam("type") String type) {
        // 禁止翻页
        if (page > 1) {
            PageInfo pageInfo = new PageInfo(new ArrayList<Goods>());
            return ResultGenerator.genSuccessResult(pageInfo);
        }
        PageHelper.startPage(1, 15);
        List<Goods> list = goodsService.selectKnowPage(goodsName, categoryId, type, isOnSale, isBuy);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
