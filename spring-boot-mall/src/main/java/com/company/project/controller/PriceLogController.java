package com.company.project.controller;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.PriceLog;
import com.company.project.service.PriceLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "金额记录模块")
@RestController
@RequestMapping("/pricelog")
public class PriceLogController {
    @Resource
    private PriceLogService priceLogService;

    @ApiOperation("金额记录新增")
    @PostMapping("/add")
    public Result add(@RequestBody PriceLog priceLog) {
        priceLogService.save(priceLog);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("金额记录删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "金额记录ID", required = true) @RequestParam Integer id) {
        priceLogService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("金额记录修改")
    @PostMapping("/update")
    public Result update(@RequestBody PriceLog priceLog) {
        priceLogService.update(priceLog);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("金额记录详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "金额记录ID", required = true) @RequestParam Integer id) {
        PriceLog priceLog = priceLogService.findById(id);
        return ResultGenerator.genSuccessResult(priceLog);
    }

    @ApiOperation("金额记录分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "用户ID", required = true) @RequestParam Integer userId) {
        PageHelper.startPage(page, size);
        List<PriceLog> list = priceLogService.page(userId);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @ApiOperation("金额记录分页")
    @GetMapping("/pageList")
    public Result pageList(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "用户ID", required = true) @RequestParam Integer userId,String username,
                       String shareName,String mobile) {
        PageHelper.startPage(page, size);
        List<PriceLog> list = priceLogService.pageList(userId, username, shareName,mobile);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
