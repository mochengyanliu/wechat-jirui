package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Footprint;
import com.company.project.service.FootprintService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "用户浏览足迹模块")
@RestController
@RequestMapping("/footprint")
public class FootprintController {
    @Resource
    private FootprintService footprintService;

    @ApiOperation("用户浏览足迹新增")
    @PostMapping("/add")
    public Result add(@RequestBody Footprint footprint) {
        footprintService.save(footprint);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户浏览足迹删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "用户浏览足迹ID", required = true) @RequestParam Integer id) {
        Footprint footprint = new Footprint();
        footprint.setId(id);
        footprint.setStatus(0);
        footprintService.update(footprint);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户浏览足迹修改")
    @PostMapping("/update")
    public Result update(@RequestBody Footprint footprint) {
        footprintService.update(footprint);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户浏览足迹详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "用户浏览足迹ID", required = true) @RequestParam Integer id) {
        Footprint footprint = footprintService.findById(id);
        return ResultGenerator.genSuccessResult(footprint);
    }

    @ApiOperation("用户浏览足迹分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Footprint> list = footprintService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
