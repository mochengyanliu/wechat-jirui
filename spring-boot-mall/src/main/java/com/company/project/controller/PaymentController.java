package com.company.project.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.*;
import com.company.project.service.*;
import com.company.project.utils.AliPayConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.Model;
import lombok.extern.log4j.Log4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Api(tags = "支付模块")
@Log4j
@RestController
@RequestMapping("payment")
public class PaymentController {

    @Resource
    private PriceLogService priceLogService;

    @Resource
    private UserService userService;
    @Resource
    private GoodsService goodsService;
    @Resource
    private AddressService addressService;
    @Resource
    private OrderService orderService;

    private static final String PRODUCT_CODE = "QUICK_WAP_WAY";


    @ApiOperation("创建订单")
    @PostMapping("/createOrder")
    public Result createOrder(@RequestBody PriceLog priceLog) {
        Order order = new Order();
        order.setOrderNo(randomOrderCode());
        Goods goods = goodsService.findById(priceLog.getGoodsId());
        Address address = addressService.findById(priceLog.getAddressId());
        order.setUserId(priceLog.getUserId());
        order.setGoodsId(priceLog.getGoodsId());
        order.setAddressId(priceLog.getAddressId());
        order.setPrice(goods.getRetailPrice());
        order.setNum(priceLog.getNum());
        order.setTotalPrice(goods.getRetailPrice().multiply(BigDecimal.valueOf(order.getNum())));
        order.setGoodsSn(goods.getGoodsSn());
        order.setGoodsName(goods.getGoodsName());
        order.setPicUrl(goods.getPicUrl());
        order.setPhone(address.getPhone());
        order.setProvince(address.getProvince());
        order.setCity(address.getCity());
        order.setDistrict(address.getDistrict());
        order.setAddress(address.getAddress());
        order.setAddressName(address.getName());
        order.setType(3);
        orderService.save(order);

        priceLog.setStatus(1);
        priceLog.setOrderNo(order.getOrderNo());
        priceLog.setName("购买赠送");
        priceLog.setPrice(order.getTotalPrice());
        priceLogService.save(priceLog);
        return ResultGenerator.genSuccessResult(order.getOrderNo());
    }

    //调起支付
    @ApiOperation("支付")
    @GetMapping("/pay")
    public void pay(@ApiParam(value = "订单号", required = true) @RequestParam String orderNo, HttpServletResponse response) throws IOException {
        // 商户订单号，商户网站订单系统中唯一订单号，必填
        PriceLog priceLog = priceLogService.selectByOrderNo(orderNo);
        if (priceLog == null){
            response.setContentType("text/html;charset=" + AliPayConfig.CHARSET);
            response.getWriter().write("支付失败，请复制链接重试！");//将表单html写到页面
            response.getWriter().flush();
            response.getWriter().close();
            return;
        }
        // 订单名称，必填
        String subject = "商品购买";
        // 商品描述，可空
        String body = "商品";

        AlipayClient client = new DefaultAlipayClient(AliPayConfig.gatewayUrl, AliPayConfig.APP_ID1, AliPayConfig.APP_PRIVATE_KEY1, AliPayConfig.FORMAT, AliPayConfig.CHARSET, AliPayConfig.ALIPAY_PUBLIC_KEY1, AliPayConfig.sign_type);
        AlipayTradeWapPayRequest alipay_request = new AlipayTradeWapPayRequest();

        String timeout_express = "2m";

        // 封装请求支付信息
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        model.setOutTradeNo(orderNo);
        model.setSubject(subject);
        model.setTotalAmount(priceLog.getMoney().toString());
        model.setBody(body);
        model.setTimeoutExpress(timeout_express);
        model.setProductCode(PRODUCT_CODE);
        alipay_request.setBizModel(model);
        // 设置异步通知地址
        alipay_request.setNotifyUrl(AliPayConfig.notify_url);
        // 设置同步地址
        alipay_request.setReturnUrl(AliPayConfig.return_url);

        // form表单生成
        String form = "";
        try {
            // 调用SDK生成表单
            form = client.pageExecute(alipay_request).getBody();

            response.setContentType("text/html;charset=" + AliPayConfig.CHARSET);
            response.getWriter().write(form);//将表单html写到页面
            response.getWriter().flush();
            response.getWriter().close();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
    }

    //支付完成后的返回
    @ApiOperation("回调")
    @GetMapping("/return")
    public String returnCall(HttpServletRequest request) throws Exception {
        // 获取支付宝GET过来反馈信息
//        Map<String, String> params = new HashMap<>();
//        Map<String, String[]> requestParams = request.getParameterMap();
//        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
//            String name = iter.next();
//            String[] values = requestParams.get(name);
//            String valueStr = "";
//            for (int i = 0; i < values.length; i++) {
//                valueStr = (i == values.length - 1) ? valueStr + values[i]
//                        : valueStr + values[i] + ",";
//            }
//            params.put(name, valueStr);
//        }
//        System.out.println("回调信息："+params);
//
//        String tradeStatus = params.get("trade_status");
//        boolean signVerified = AlipaySignature.rsaCheckV1(params, AliPayConfig.ALIPAY_PUBLIC_KEY1, AliPayConfig.CHARSET, AliPayConfig.sign_type); //调用SDK验证签名
//        if (signVerified) {
//            if (tradeStatus.equals("TRADE_SUCCESS")) {
//                PriceLog priceLog = priceLogService.selectByOrderNo(params.get("out_trade_no"));
//                if (priceLog.getStatus() == 1) {
//                    User user = userService.findById(priceLog.getUserId());
//                    priceLog.setStatus(2);
//                    priceLog.setBeforPrice(user.getMoney());
//                    priceLog.setAfterPrice(user.getMoney().add(priceLog.getPrice()));
//                    priceLogService.update(priceLog);
//
//                    userService.updateMoney(priceLog.getPrice(), user.getId());
//                    Order order = orderService.findBy("orderNo", priceLog.getOrderNo());
//                    System.out.println("订单状态：" + order.getStatus());
//                    if (order.getStatus() == 1) {
//                        System.out.println("修改状态：" + order.getStatus());
//                        order.setStatus(2);
//                        orderService.update(order);
//                    }
//                    log.info("return sign  success");
//                }
//                return "支付成功，请查看余额变化！";
//            }
            return "支付成功，请查看余额变化！";
//        } else {
//            log.info("return sign  failed");
//            return "支付成功失败，请重试！";
//        }
    }

/*
*
*
* TRADE_SUCCESS状态代表了充值成功，也就是说钱已经进了支付宝（担保交易）或卖家（即时到账）；
* 这时候，这笔交易应该还可以进行后续的操作（比如三个月后交易状态自动变成TRADE_FINISHED），
* 因为整笔交易还没有关闭掉，也就是说一定还有主动通知过来。
* 而TRADE_FINISHED代表了这笔订单彻底完成了，不会再有任何主动通知过来了。

综上所述，收到TRADE_FINISHED请求后，这笔订单就结束了，支付宝不会再主动请求商户网站了；
* 收到TRADE_SUCCESS请求后，后续一定还有至少一条通知记录，即TRADE_FINISHED。
* 所以，在做通知接口时，切记使用判断订单状态用或的关系
*
*
* */

    //异步通知
    @ApiOperation("异步通知")
    @PostMapping("/notify")
    public String notifyCall(HttpServletRequest request, HttpSession session, Model model) throws Exception {
        // 获取支付宝反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        System.out.println("异步通知："+params);

        String tradeStatus = params.get("trade_status");
        boolean signVerified = AlipaySignature.rsaCheckV1(params, AliPayConfig.ALIPAY_PUBLIC_KEY1, AliPayConfig.CHARSET, AliPayConfig.sign_type); //调用SDK验证签名

        if (signVerified) {
            if (tradeStatus.equals("TRADE_SUCCESS")){
                PriceLog priceLog = priceLogService.selectByOrderNo(params.get("out_trade_no"));
                if (priceLog.getStatus()==1){
                    User user = userService.findById(priceLog.getUserId());
                    priceLog.setStatus(2);
                    priceLog.setBeforPrice(user.getMoney());
                    priceLog.setAfterPrice(user.getMoney().add(priceLog.getPrice()));
                    priceLogService.update(priceLog);

                    userService.updateMoney(priceLog.getPrice(),user.getId());
                    Order order = orderService.findBy("orderNo", priceLog.getOrderNo());
                    System.out.println("订单状态："+order.getStatus());
                    if (order.getStatus()==1){
                        System.out.println("修改状态："+order.getStatus());
                        order.setStatus(2);
                        orderService.update(order);
                    }
                }
            }
            /*
            if(trade_status.equals("TRADE_FINISHED")){
            } else if (trade_status.equals("TRADE_SUCCESS")){
            }
             */
            return "success";
        } else {
            return "fail";
        }
    }
    public static String randomOrderCode() {
        SimpleDateFormat dmDate = new SimpleDateFormat("yyyyMMddHHmmss");
        Random r = new Random();
        StringBuilder rs = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            rs.append(r.nextInt(10));
        }
        String randata = rs.toString();
        Date date = new Date();
        String dateran = dmDate.format(date);
        String Xsode = "NO" + dateran + randata;
        if (Xsode.length() < 24) {
            Xsode = Xsode + 0;
        }
        return Xsode;
    }
}
