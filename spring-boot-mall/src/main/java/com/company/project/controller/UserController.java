package com.company.project.controller;

import com.alibaba.fastjson.JSONObject;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Dict;
import com.company.project.model.PriceLog;
import com.company.project.model.User;
import com.company.project.service.DictService;
import com.company.project.service.PriceLogService;
import com.company.project.service.UserService;
import com.company.project.vo.CountVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by CodeGenerator on 2023/03/02.
 */

@Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class UserController {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private UserService userService;
    @Resource
    private PriceLogService priceLogService;
    @Resource
    private DictService dictService;
//    private static String appid= "wx8fba234f9d7bce4e";
//    private static String appid= "wx1eec76cdf579c297";
    private static String appid= "wxaddc294dbbfb46ab";
//    private static String secret="b43867ef35d0359eb573d35f8a0ba576";
//    private static String secret="e09d945706380a21c9da4b25c89c77a6";
    private static String secret="16748d0bdde3c87e3a09c83c489f6d1d";

    @ApiOperation("获取TOKEN")
    @GetMapping("/getToken")
    public Result getToken(@ApiParam(value = "用户标志凭证", required = true) @RequestParam String code, @ApiParam(value = "用户标志凭证", required = true) @RequestParam Integer shareId) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secret;
        JSONObject jsonObject = restTemplate.getForObject(url, JSONObject.class);
        String accessToken = jsonObject.getString("access_token");

        url = "https://api.weixin.qq.com/sns/jscode2session?appid="+appid+"&secret="+secret+"&js_code=" + code + "&grant_type=authorization_code";
        Map<String, Object> map = new HashMap<>();
        map.put("code", code);
        String result = restTemplate.getForObject(url, String.class);
        jsonObject = JSONObject.parseObject(result);
        if (jsonObject.getString("openid") != null) {
            String openid = jsonObject.getString("openid");
            User user = userService.findBy("wxOpenid", openid);

            if (user == null) {
                user = new User();
                user.setUsername("请完善个人信息");
                user.setWxOpenid(openid);
                user.setPassword("123456");
                user.setGender("男");
                user.setType(1);
                user.setShareId(shareId);
                user.setMoney(new BigDecimal(0));
                user.setGold(new BigDecimal(0));
                user.setRate(new BigDecimal(0.88));
                user.setFanli(new BigDecimal(0));
                userService.save(user);
            }
            user.setSession(accessToken);
            if (StringUtils.isNotEmpty(user.getImage())) {
                user.setImageUrl(picurl + user.getImage());
            }
            return ResultGenerator.genSuccessResult(user);
        } else {
            return ResultGenerator.genFailResult("code获取失败：" + jsonObject);
        }
    }

    @ApiOperation("获取手机号")
    @GetMapping("/getPhone")
    public Result getPhone(@ApiParam(value = "用户标志凭证", required = true) @RequestParam String code,
                           @ApiParam(value = "用户ID", required = true) @RequestParam Integer userId,
                           @ApiParam(value = "省份", required = true) @RequestParam String province,
                           @ApiParam(value = "城市", required = true) @RequestParam String city) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secret;
        JSONObject jsonObject = restTemplate.getForObject(url, JSONObject.class);
        String accessToken = jsonObject.getString("access_token");

        JSONObject object = new JSONObject();
        object.put("code", code);
        User user = userService.findById(userId);

        url = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=" + accessToken;
        jsonObject = restTemplate.postForObject(url, object, JSONObject.class);
        if (user != null) {
            JSONObject phoneInfo = jsonObject.getJSONObject("phone_info");
            if (phoneInfo == null){
                return ResultGenerator.genFailResult("获取手机号失败");
            }
            user.setMobile(phoneInfo.getString("purePhoneNumber"));
            user.setProvince(province);
            user.setCity(city);
            userService.update(user);
            if (StringUtils.isNotEmpty(user.getImage())) {
                user.setImageUrl(picurl + user.getImage());
            }
        } else {
            return ResultGenerator.genFailResult("用户不存在");
        }
        return ResultGenerator.genSuccessResult(user);
    }

    @ApiOperation("获取二维码")
    @GetMapping("/getQrCode")
    public Result getQrCode(@ApiParam(value = "用户标志凭证", required = true) @RequestParam String code,
                            @ApiParam(value = "用户ID", required = true) @RequestParam Integer userId) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secret;
        JSONObject jsonObject = restTemplate.getForObject(url, JSONObject.class);
        String accessToken = jsonObject.getString("access_token");
        JSONObject object = new JSONObject();
        object.put("page", "pages/allShop/shop/shop");
        object.put("scene", "id=" + userId);
        object.put("check_path", false);
        object.put("env_version", "release");
        HttpEntity httpEntity = new HttpEntity(object, new HttpHeaders());
        url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken;
        ResponseEntity<byte[]> result = restTemplate.exchange(url, HttpMethod.POST, httpEntity, byte[].class);
        String fileName = UUID.randomUUID().toString().replaceAll("-", "") + "分享二维码.jpg";
        try (FileOutputStream out = new FileOutputStream(new File("src/main/resources/static/images/" + fileName))) {
            out.write(result.getBody(), 0, result.getBody().length);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultGenerator.genSuccessResult(picurl + "/static/images/" + fileName);
    }

    @ApiOperation("用户新增")
    @PostMapping("/add")
    public Result add(@ApiParam(value = "用户实体", required = true) @RequestBody User user) {
        user.setStatus(1);
        user.setMoney(new BigDecimal(0));
        user.setGold(new BigDecimal(0));
        if (user.getFanli() == null) {
            user.setFanli(new BigDecimal(0));
        }
        if (user.getRate()==null){
            user.setRate(new BigDecimal("0.88"));
        }
        userService.save(user);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "用户ID", required = true) @RequestParam Integer id) {
        User user = new User();
        user.setId(id);
        user.setStatus(0);
        userService.update(user);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户修改")
    @PostMapping("/update")
    public Result update(@ApiParam(value = "用户实体", required = true) @RequestBody User user) {
        User oldUser = userService.findById(user.getId());
        if (user.getAddMoney() != null && user.getAddMoney().compareTo(BigDecimal.ZERO) != 0) {
//            user.getMoney().add(user.getAddMoney());
            PriceLog priceLog = new PriceLog();
            priceLog.setUserId(user.getId());
            priceLog.setPrice(user.getAddMoney());
            priceLog.setBeforPrice(oldUser.getMoney());
            if (user.getAddMoney().compareTo(BigDecimal.ZERO) > 0){
                priceLog.setName("活动赠送");
            } else {
                priceLog.setName("余额扣除");

            }
            if (user.getAddType()==1) {
                priceLog.setAfterPrice(oldUser.getMoney().add(user.getAddMoney()));
                user.setMoney(oldUser.getMoney().add(user.getAddMoney()));
            } else {
                user.setGold(oldUser.getGold().add(user.getAddMoney()));
                priceLog.setAfterPrice(oldUser.getMoney());
            }
            priceLog.setChecked(4);
            //时间（精确到毫秒）
            DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
            String localDate = LocalDateTime.now().format(ofPattern);
            //随机数
            String randomNumeric = RandomStringUtils.randomNumeric(4);
            String orderNo = "NO" + localDate + randomNumeric;
            priceLog.setOrderNo(orderNo);
            priceLog.setType(user.getAddType());
            priceLogService.save(priceLog);

        }
//        User old = userService.findById(user.getId());
//        if (old.getImage() != null && !old.getImage().equals(user.getImage()) || !old.getUsername().equals(user.getUsername())) {
//            user.setUpdateImage(user.getImage());
//            user.setUpdateUsername(user.getUsername());
//            user.setCheckUpdate(1);
//        }
//        user.setImage(null);
//        user.setUsername(null);
        userService.update(user);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "用户ID", required = true) @RequestParam Integer id) {
        User user = userService.findById(id);
        if (StringUtils.isNotEmpty(user.getImage())) {
            user.setImageUrl(picurl + user.getImage());
        }
        return ResultGenerator.genSuccessResult(user);
    }

    @ApiOperation("用户分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "用户名", required = true) @RequestParam("username") String userName,
                       String mobile,Integer isRobot, String shareName, String shareMobile, Integer type) {
        PageHelper.startPage(page, size);
        List<User> list = userService.page(userName, mobile, isRobot, shareName, shareMobile,type);
        for (User user : list) {
            if (StringUtils.isNotEmpty(user.getImage())) {
                user.setImageUrl(picurl + user.getImage());
            }
            if (StringUtils.isNotEmpty(user.getUpdateImage())) {
                user.setUpdateImageUrl(picurl + user.getUpdateImage());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }


    @ApiOperation("所有用户")
    @GetMapping("/list")
    public Result list() {
        Condition condition = new Condition(User.class);
        condition.createCriteria().andEqualTo("status", 1);
        List<User> list = userService.findByCondition(condition);
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation("用户审核")
    @GetMapping("/check")
    public Result check(@ApiParam(value = "用户ID", required = true) @RequestParam Integer userId,
                        @ApiParam(value = "审核状态", required = true) @RequestParam Integer status) {
        User user = userService.findById(userId);
        if (status == 2) {
            user.setImage(user.getUpdateImage());
            user.setUsername(user.getUpdateUsername());
            user.setUpdateUsername("");
            user.setUpdateImage("");
            user.setCheckUpdate(2);
            userService.checkUserInfo(user);
        } else {
            user.setUpdateUsername("");
            user.setUpdateImage("");
            user.setCheckUpdate(3);
            userService.checkUserInfo(user);
        }
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户邀请")
    @GetMapping("/invite")
    public Result invite(@ApiParam(value = "用户ID", required = true) @RequestParam Integer userId) {
        Map<String, Object> map = new HashMap<>();

        Condition condition = new Condition(User.class);
        condition.createCriteria().andEqualTo("status", 1).andEqualTo("shareId", userId);
        List<User> users = userService.findByCondition(condition);
        for (User user : users) {
            if (StringUtils.isNotEmpty(user.getImage())) {
                user.setImageUrl(picurl + user.getImage());
            }
            if (StringUtils.isEmpty(user.getMobile())){
                user.setMobile("");
            }
        }
//        List<PriceLog> priceLogs = priceLogService.getByShareId(userId);
//        List<Integer> user =  priceLogs.stream().map(PriceLog::getUserId).collect(Collectors.toList());
//        List<Integer> result=new LinkedList<>();
//        if (user.size()>0&&user!=null){
//            // 新建HashSet集合，将list放入
//            Set<Integer> set=new HashSet<>();
//            set.addAll(user);
//
//            // 将去重后的set集合放入结果集合result中
//            result.addAll(set);
//        }
//        map.put("priceLogs", priceLogs);
//        map.put("count", result.size());
//        BigDecimal price = new BigDecimal(0);
//        for (PriceLog priceLog : priceLogs) {
//            price = price.add(priceLog.getPrice());
//            if (StringUtils.isNotEmpty(priceLog.getImage())) {
//                priceLog.setImageUrl(picurl + priceLog.getImage());
//            }
//        }
//        map.put("price", price);
        map.put("users",users);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("用户签到")
    @GetMapping("/signin")
    public Result signin(@ApiParam(value = "用户ID", required = true) @RequestParam Integer userId) {
        User user = userService.findById(userId);
        Calendar calendar = Calendar.getInstance();
        // 获取今天星期几
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        Integer type = 2;
        if (dayOfWeek == 5) {
            type = 1;
        }
        PriceLog priceLog = priceLogService.selectTodayByUserId(userId);
        if (priceLog != null) {
            return ResultGenerator.genFailResult("今日已签到");
        }
        priceLog = new PriceLog();

        // 添加用户余额
        if (type == 1) {
            userService.updateMoney(new BigDecimal(3), userId);
            priceLog.setPrice(new BigDecimal(3));
        } else if (type == 2) {
            userService.updateGold(new BigDecimal(5), userId);
            priceLog.setPrice(new BigDecimal(5));
        }
        priceLog.setUserId(userId);
        priceLog.setChecked(4);
        priceLog.setBeforPrice(user.getMoney());
        if (type == 1) {
            priceLog.setType(3);
            priceLog.setName("渔乐券签到");
            priceLog.setAfterPrice(user.getMoney().add(new BigDecimal(3)));
        } else if (type == 2) {
            priceLog.setType(4);
            priceLog.setName("金币签到");
            priceLog.setAfterPrice(user.getMoney());
        }
        //时间（精确到毫秒）
        DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        String localDate = LocalDateTime.now().format(ofPattern);
        //随机数
        String randomNumeric = RandomStringUtils.randomNumeric(4);
        String orderNo = "NO" + localDate + randomNumeric;
        priceLog.setOrderNo(orderNo);
        priceLogService.save(priceLog);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("用户今日签到")
    @GetMapping("/todaySignin")
    public Result todaySignin(@ApiParam(value = "用户ID", required = true) @RequestParam Integer userId) {// 创建一个Calendar实例
        Calendar calendar = Calendar.getInstance();
        // 获取今天星期几
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        PriceLog priceLog = priceLogService.selectTodayByUserId(userId);
        User user = userService.findById(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("today", priceLog);
        map.put("gold", user.getGold());
        map.put("dayOfWeek", dayOfWeek);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("支付启用")
    @GetMapping("/isPay")
    public Result isPay() {
        Dict dict = dictService.selectByCode("isPay");
        return ResultGenerator.genSuccessResult(Boolean.valueOf(dict.getValue()));
    }

    @ApiOperation("查询中奖启用")
    @GetMapping("/isOpen")
    public Result isOpen() {
        Dict dict = dictService.selectByCode("isOpen");
        return ResultGenerator.genSuccessResult(dict);
    }
    @ApiOperation("修改中奖启用")
    @PostMapping("/updateOpen")
    public Result updateOpen(@RequestBody Dict dict) {
        dictService.update(dict);
        return ResultGenerator.genSuccessResult(dict);
    }

    @ApiOperation("统计查询")
    @GetMapping("countPage")
    public Result countPage(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                            @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                            @ApiParam(value = "用户名", required = true) String userName,
                            String mobile,String shareName, String beginTime, String endTime) {
        PageHelper.startPage(page, size);
        List<CountVO> list = userService.countPage(userName, mobile,shareName, beginTime, endTime);
        PageInfo pageInfo = new PageInfo(list);
        pageInfo.setTotal(userService.countSize(userName, mobile,shareName));
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
