package com.company.project.controller;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Cashout;
import com.company.project.model.Order;
import com.company.project.model.PriceLog;
import com.company.project.model.User;
import com.company.project.service.CashoutService;
import com.company.project.service.OrderService;
import com.company.project.service.PriceLogService;
import com.company.project.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Api(tags = "提现模块")
@RestController
@RequestMapping("/cashout")
public class CashoutController {
    @Resource
    private CashoutService cashoutService;
    @Resource
    private UserService userService;
    @Resource
    private PriceLogService priceLogService;
    @Resource
    private OrderService orderService;

    @ApiOperation("提现新增")
    @PostMapping("/add")
    public Result add(@RequestBody Cashout cashout) {
        if (cashout.getMoney() == null) {
            return ResultGenerator.genFailResult("请输入提现金额");
        } else if (StringUtils.isEmpty(cashout.getAccount())) {
            return ResultGenerator.genFailResult("请输入支付宝账号");
        } else if (StringUtils.isEmpty(cashout.getUsername())) {
            return ResultGenerator.genFailResult("请输入真实姓名");
        }
        User user = userService.findById(cashout.getUserId());
        if (user.getMoney().compareTo(cashout.getMoney()) < 0) {
            return ResultGenerator.genFailResult("渔乐券不足");
        }
        Condition condition = new Condition(Order.class);
        condition.createCriteria().andEqualTo("type", 1).andEqualTo("userId", cashout.getUserId());
        List<Order> orders = orderService.findByCondition(condition);
        if (orders.size() == 0) {
            return ResultGenerator.genFailResult("至少消费一笔才能提现");
        }
        cashout.setStatus(1);
        cashoutService.save(cashout);
        userService.updateMoney(cashout.getMoney().negate(), cashout.getUserId());
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("提现删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "提现ID", required = true) @RequestParam Integer id) {
        Cashout cashout = cashoutService.findById(id);
        if (cashout.getStatus() != 1) {
            return ResultGenerator.genFailResult("当前状态不允许删除");
        }
        cashout.setId(id);
        cashout.setStatus(0);
        cashoutService.update(cashout);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("提现修改")
    @PostMapping("/update")
    public Result update(@RequestBody Cashout cashout) {
        if (cashout.getStatus() == 2) {
//            User user = userService.findById(cashout.getUserId());
//            if (user.getMoney().compareTo(cashout.getMoney()) < 0) {
//                return ResultGenerator.genFailResult("渔乐券不足");
//            }
//            userService.updateMoney(cashout.getMoney().negate(), cashout.getUserId());
            // 生成价格记录
            PriceLog priceLog = new PriceLog();
            priceLog.setPrice(cashout.getMoney().negate());
            priceLog.setUserId(cashout.getUserId());
            priceLog.setName("提现扣除");
            priceLog.setType(1);
            //时间（精确到毫秒）
            DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
            String localDate = LocalDateTime.now().format(ofPattern);
            //随机数
            String randomNumeric = RandomStringUtils.randomNumeric(4);
            String orderNo = "NO" + localDate + randomNumeric;
            priceLog.setOrderNo(orderNo);
            priceLogService.save(priceLog);
        } else {
            userService.updateMoney(cashout.getMoney(), cashout.getUserId());
        }
        cashoutService.update(cashout);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("提现详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "提现ID", required = true) @RequestParam Integer id) {
        Cashout cashout = cashoutService.findById(id);
        return ResultGenerator.genSuccessResult(cashout);
    }

    @ApiOperation("提现分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "用户ID", required = true) Integer userId,
                       @ApiParam(value = "真实姓名", required = true) String userName) {
        PageHelper.startPage(page, size);
        List<Cashout> list = cashoutService.page(userId, userName);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
