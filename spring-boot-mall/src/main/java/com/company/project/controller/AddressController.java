package com.company.project.controller;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Address;
import com.company.project.service.AddressService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Api(tags = "收货地址模块")
@RestController
@RequestMapping("/address")
public class AddressController {
    @Resource
    private AddressService addressService;

    @ApiOperation("收货地址新增")
    @PostMapping("/add")
    public Result add(@RequestBody Address address) {
        Condition condition = new Condition(Address.class);
        condition.createCriteria().andEqualTo("status", 1).andEqualTo("userId",address.getUserId());
        List<Address> addresses = addressService.findByCondition(condition);
        if (addresses.size()==0){
            address.setIsDefault(1);
        }
        addressService.save(address);
        if (address.getIsDefault() == 1) {
            addressService.updateIsDefaultByUserId(address.getId(), address.getUserId());
        }
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("收货地址删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "收货地址ID", required = true) @RequestParam Integer id) {
        Address address = new Address();
        address.setId(id);
        address.setStatus(0);
        addressService.update(address);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("收货地址修改")
    @PostMapping("/update")
    public Result update(@RequestBody Address address) {
        addressService.update(address);
        if (address.getIsDefault() == 1) {
            addressService.updateIsDefaultByUserId(address.getId(), address.getUserId());
        }
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("收货地址详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "收货地址ID", required = true) @RequestParam Integer id) {
        Address address = addressService.findById(id);
        return ResultGenerator.genSuccessResult(address);
    }

    @ApiOperation("收货地址分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "姓名", required = true) @RequestParam("name") String name,
                       @ApiParam(value = "用户ID", required = true) @RequestParam Integer userId) {
        PageHelper.startPage(page, size);
        List<Address> list = addressService.page(userId, name);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }


    @ApiOperation("用户地址")
    @GetMapping("/selectByUserId")
    public Result selectByUserId(@ApiParam(value = "用户ID", required = true) @RequestParam Integer userId,
                                 @ApiParam(value = "是否默认（0-非默认，1-默认，全部-空）", required = true) @RequestParam Integer isDefault) {
        List<Address> addresses = addressService.selectByUserId(userId, isDefault);
        return ResultGenerator.genSuccessResult(addresses);
    }
}
