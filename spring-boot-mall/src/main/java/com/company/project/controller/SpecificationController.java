package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Specification;
import com.company.project.service.SpecificationService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "商品规格模块")
@RestController
@RequestMapping("/specification")
public class SpecificationController {
    @Resource
    private SpecificationService specificationService;

    @ApiOperation("商品规格新增")
    @PostMapping("/add")
    public Result add(@RequestBody Specification specification) {
        specificationService.save(specification);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("商品规格删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "商品规格ID", required = true) @RequestParam Integer id) {
        Specification specification = new Specification();
        specification.setId(id);
        specification.setStatus(0);
        specificationService.update(specification);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("商品规格修改")
    @PostMapping("/update")
    public Result update(@RequestBody Specification specification) {
        specificationService.update(specification);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("商品规格详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "商品规格ID", required = true) @RequestParam Integer id) {
        Specification specification = specificationService.findById(id);
        return ResultGenerator.genSuccessResult(specification);
    }

    @ApiOperation("商品规格分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Specification> list = specificationService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
