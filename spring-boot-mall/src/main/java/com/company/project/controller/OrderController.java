package com.company.project.controller;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.*;
import com.company.project.service.*;
import com.company.project.utils.RedisUtil;
import com.company.project.vo.OrderVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Log4j2
@Api(tags = "订单模块")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private OrderService orderService;
    @Resource
    private GoodsService goodsService;
    @Resource
    private AddressService addressService;
    @Resource
    private UserService userService;
    @Resource
    private PriceLogService priceLogService;
    @Resource
    private CartService cartService;
    @Resource
    private DictService dictService;
    @Autowired
    RedisUtil redisUtil;

    @ApiOperation("订单新增")
    @PostMapping("/add")
    public Result add(@RequestBody OrderVo orderVo) {
        if (redisUtil.hasKey(orderVo.getUserId().toString())) {
            return ResultGenerator.genFailResult("5秒内不可重复提交订单");
        } else {
            redisUtil.set(orderVo.getUserId().toString(), "lock", 5);
        }
        synchronized (this) {
            BigDecimal totalPrice = new BigDecimal(0);
            for (Map<String, String> good : orderVo.getGoods()) {
                Goods goods = goodsService.findById(Integer.parseInt(good.get("id")));
                totalPrice = totalPrice.add(goods.getRetailPrice().multiply(BigDecimal.valueOf(Integer.parseInt(good.get("num")))));
                if (goods.getCounterPrice().compareTo(goods.getTotalPrice().add(totalPrice)) < 0) {
                    redisUtil.del(orderVo.getUserId().toString());
                    return ResultGenerator.genFailResult("商品数量不够");
                }
            }
            User user = userService.findById(orderVo.getUserId());
            if (orderVo.getType() == 1 && user.getMoney().compareTo(totalPrice) < 0) {
                redisUtil.del(orderVo.getUserId().toString());
                return ResultGenerator.genFailResult("渔乐券不足");
            } else if (orderVo.getType() == 2 && user.getGold().compareTo(totalPrice) < 0) {
                redisUtil.del(orderVo.getUserId().toString());
                return ResultGenerator.genFailResult("金币不足");
            }
            // 上级返消费券
//            if (orderVo.getType() == 1 && user.getShareId() != null) {
//                Condition condition = new Condition(Order.class);
//                condition.createCriteria().andEqualTo("userId", user.getId());
//                List<Order> orders = orderService.findByCondition(condition);
//                if (orders.size() == 0) {
//                    User shareUser = userService.findById(user.getShareId());
//                    userService.updateMoney(new BigDecimal(5), user.getShareId());
//                    PriceLog priceLog = new PriceLog();
//                    priceLog.setType(orderVo.getType());
//                    priceLog.setPrice(new BigDecimal(5));
//                    priceLog.setUserId(orderVo.getUserId());
//                    priceLog.setName("邀请返券");
//                    priceLog.setShareId(user.getShareId());
//                    priceLog.setChecked(4);
//                    priceLog.setBeforPrice(shareUser.getMoney());
//                    priceLog.setAfterPrice(shareUser.getMoney().add(new BigDecimal(5)));
//                    //时间（精确到毫秒）
//                    DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
//                    String localDate = LocalDateTime.now().format(ofPattern);
//                    //随机数
//                    String randomNumeric = RandomStringUtils.randomNumeric(4);
//                    String orderNo = "NO" + localDate + randomNumeric;
//                    priceLog.setOrderNo(orderNo);
//                    priceLogService.save(priceLog);
//                }
//            }
            for (Map<String, String> good : orderVo.getGoods()) {
                Order order = new Order();
                order.setOrderNo(randomOrderCode());
                Goods goods = goodsService.findById(Integer.parseInt(good.get("id")));
                Address address = addressService.findById(orderVo.getAddressId());
                order.setUserId(orderVo.getUserId());
                order.setGoodsId(Integer.parseInt(good.get("id")));
                order.setAddressId(orderVo.getAddressId());
                order.setPrice(goods.getRetailPrice());
                order.setNum(Integer.parseInt(good.get("num")));
                order.setTotalPrice(goods.getRetailPrice().multiply(BigDecimal.valueOf(order.getNum())));
                order.setGoodsSn(goods.getGoodsSn());
                order.setGoodsName(goods.getGoodsName());
                order.setPicUrl(goods.getPicUrl());
                order.setPhone(address.getPhone());
                order.setProvince(address.getProvince());
                order.setCity(address.getCity());
                order.setDistrict(address.getDistrict());
                order.setAddress(address.getAddress());
                order.setAddressName(address.getName());
                order.setType(orderVo.getType());
                order.setBeginNum(10000001 + goods.getTotalPrice().divide(goods.getRetailPrice()).intValue());
                order.setEndNum(10000000 + goods.getTotalPrice().divide(goods.getRetailPrice()).intValue() + order.getNum());

                String[] strs = goods.getRandomList().split(",");
                List<Integer> list = new ArrayList<>();
                int base = goods.getTotalPrice().divide(goods.getRetailPrice()).intValue();
                for (int i = base; i < base+order.getNum(); i++) {
                    list.add(10000000+Integer.valueOf(strs[i]));
                }
                order.setRandom(StringUtils.join(list,","));
                orderService.save(order);
                // 修改商品总价格
                goodsService.udpateTotalPrice(order.getTotalPrice(), 0, Integer.parseInt(good.get("id")));
                // 生成价格记录
                PriceLog priceLog = new PriceLog();
                priceLog.setPrice(order.getTotalPrice().negate());
                priceLog.setUserId(orderVo.getUserId());
                priceLog.setName("购买商品");
                priceLog.setType(orderVo.getType());
                priceLog.setBeforPrice(user.getMoney());
                priceLog.setAfterPrice(user.getMoney().subtract(order.getTotalPrice()));
                //时间（精确到毫秒）
                DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
                String localDate = LocalDateTime.now().format(ofPattern);
                //随机数
                String randomNumeric = RandomStringUtils.randomNumeric(4);
                String orderNo = "NO" + localDate + randomNumeric;
                priceLog.setOrderNo(orderNo);
                priceLogService.save(priceLog);

                // 扣除用户余额
                if (orderVo.getType() == 1) {
                    userService.updateMoney(order.getTotalPrice().negate(), order.getUserId());
                } else if (orderVo.getType() == 2) {
                    userService.updateGold(order.getTotalPrice().negate(), order.getUserId());
                }
                // 删除购物车
                if (StringUtils.isNotEmpty(good.get("cartId"))) {
                    cartService.deleteById(Integer.valueOf(good.get("cartId")));
                }
                //上级返利
                if (orderVo.getType() == 1 && user.getShareId() != null) {
//                    Dict dict = dictService.selectByCode("rebate");
                    User shareUser = userService.findById(user.getShareId());

                    userService.updateMoney(order.getTotalPrice().multiply(shareUser.getFanli()), user.getShareId());

                    priceLog = new PriceLog();
                    priceLog.setType(orderVo.getType());
                    priceLog.setPrice(order.getTotalPrice().multiply(shareUser.getFanli()));
                    priceLog.setUserId(orderVo.getUserId());
                    priceLog.setName("消费返利（"+user.getUsername()+"-"+user.getMobile()+")");
                    priceLog.setShareId(user.getShareId());
                    priceLog.setChecked(4);
                    priceLog.setBeforPrice(shareUser.getMoney());
                    priceLog.setAfterPrice(shareUser.getMoney().add(order.getTotalPrice().multiply(shareUser.getFanli())));
                    //时间（精确到毫秒）
                    ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
                    localDate = LocalDateTime.now().format(ofPattern);
                    //随机数
                    randomNumeric = RandomStringUtils.randomNumeric(4);
                    orderNo = "NO" + localDate + randomNumeric;
                    priceLog.setOrderNo(orderNo);
                    priceLogService.save(priceLog);
                }
            }
        }
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("订单删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "订单ID", required = true) @RequestParam Integer id) {
        Order order = new Order();
        order.setId(id);
        order.setStatus(0);
        orderService.update(order);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("订单修改")
    @PostMapping("/update")
    public Result update(@RequestBody Order order) {
        orderService.update(order);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("订单详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "订单ID", required = true) @RequestParam Integer id) {
        Order order = orderService.selectById(id);
        return ResultGenerator.genSuccessResult(order);
    }

    @ApiOperation("订单分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "订单号", required = false) @RequestParam("orderNo") String orderNo,
                       @ApiParam(value = "订单用户", required = false) @RequestParam("userId") Integer userId,
                       @ApiParam(value = "订单状态", required = false) @RequestParam("status") Integer status,
                       String username, Integer isWin, Integer isRobot, String mobile) {
        PageHelper.startPage(page, size);
        List<Order> list = orderService.page(orderNo, userId, status, username, isWin, isRobot, mobile);
        for (Order order : list) {
            if (StringUtils.isNotEmpty(order.getPicUrl())) {
                order.setPicUrlSrc(picurl + order.getPicUrl());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @ApiOperation("商品换取记录")
    @GetMapping("/listByGoodsId")
    public Result listByGoodsId(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                                @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                                @ApiParam(value = "商品ID", required = false) @RequestParam("goodsId") Integer goodsId) {
        PageHelper.startPage(page, size);
        List<Order> list = orderService.listByGoodsId(goodsId);
        for (Order order : list) {
            if (StringUtils.isNotEmpty(order.getPicUrl())) {
                order.setPicUrlSrc(picurl + order.getPicUrl());
            }
            if (StringUtils.isNotEmpty(order.getImage())) {
                order.setImageSrc(picurl + order.getImage());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @ApiOperation("一键代售")
    @PostMapping("/sell")
    public Result sell(@RequestBody Order order) {
        if (redisUtil.hasKey(order.getId().toString()+order.getUserId().toString())) {
            return ResultGenerator.genFailResult("5秒内不可重复置换");
        } else {
            redisUtil.set(order.getId().toString()+order.getUserId().toString(), "lock", 5);
        }
        synchronized (this) {
            order = orderService.selectById(order.getId());
            User user = userService.findById(order.getUserId());

            PriceLog priceLog = new PriceLog();
            priceLog.setUserId(user.getId());

            DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
            String localDate = LocalDateTime.now().format(ofPattern);
            //时间（精确到毫秒）
            ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
            localDate = LocalDateTime.now().format(ofPattern);
            //随机数
            String randomNumeric = RandomStringUtils.randomNumeric(4);
            String orderNo = "NO" + localDate + randomNumeric;
            priceLog.setOrderNo(orderNo);
            priceLog.setStatus(2);
            priceLog.setCreateTime(new Date());
            priceLog.setChecked(4);
            priceLog.setType(1);
            priceLog.setBeforPrice(user.getMoney());
            if (order.getType() == 1) {
                // 积分
                BigDecimal price = order.getCounterPrice().multiply(user.getRate());
                userService.updateMoney(price, user.getId());
                priceLog.setPrice(price);
                priceLog.setAfterPrice(user.getMoney().add(price));
                priceLog.setName("活动赠送");
            } else if (order.getType() == 3) {
                // 购买
                BigDecimal price = order.getCounterPrice().subtract(order.getRetailPrice());
                price = price.multiply(new BigDecimal(order.getNum()));
                userService.updateMoney(price, user.getId());
                priceLog.setPrice(price);
                priceLog.setAfterPrice(user.getMoney().add(price));
                priceLog.setName("购买赠送");
            }
            order.setStatus(4);
            orderService.update(order);
            priceLogService.save(priceLog);
            return ResultGenerator.genSuccessResult();
        }
    }

    public static String randomOrderCode() {
        SimpleDateFormat dmDate = new SimpleDateFormat("yyyyMMddHHmmss");
        Random r = new Random();
        StringBuilder rs = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            rs.append(r.nextInt(10));
        }
        String randata = rs.toString();
        Date date = new Date();
        String dateran = dmDate.format(date);
        String Xsode = "NO" + dateran + randata;
        if (Xsode.length() < 24) {
            Xsode = Xsode + 0;
        }
        return Xsode;
    }
}
