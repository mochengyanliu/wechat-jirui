package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Brand;
import com.company.project.service.BrandService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "品牌商模块")
@RestController
@RequestMapping("/brand")
public class BrandController {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private BrandService brandService;

    @ApiOperation("品牌商新增")
    @PostMapping("/add")
    public Result add(@RequestBody Brand brand) {
        brandService.save(brand);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("品牌商删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "品牌商ID", required = true) @RequestParam Integer id) {
        Brand brand = new Brand();
        brand.setId(id);
        brand.setStatus(0);
        brandService.update(brand);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("品牌商修改")
    @PostMapping("/update")
    public Result update(@RequestBody Brand brand) {
        brandService.update(brand);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("品牌商详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "品牌商ID", required = true) @RequestParam Integer id) {
        Brand brand = brandService.findById(id);
        return ResultGenerator.genSuccessResult(brand);
    }

    @ApiOperation("品牌商分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "品牌商名称", required = true) @RequestParam("brandName") String brandName) {
        PageHelper.startPage(page, size);
        Condition condition = new Condition(Brand.class);
        condition.createCriteria().andEqualTo("status", 1).andLike("brandName", "%"+brandName+"%");
        List<Brand> list = brandService.findByCondition(condition);
        for (Brand brand : list) {
            if (StringUtils.isNotEmpty(brand.getPicUrl())) {
                brand.setPicUrlSrc(picurl + brand.getPicUrl());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @ApiOperation("所有品牌商")
    @GetMapping("/list")
    public Result list() {
        List<Brand> list = brandService.findAll();
        return ResultGenerator.genSuccessResult(list);
    }
}
