package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Collect;
import com.company.project.service.CollectService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "收藏模块")
@RestController
@RequestMapping("/collect")
public class CollectController {
    @Resource
    private CollectService collectService;

    @ApiOperation("收藏新增")
    @PostMapping("/add")
    public Result add(@RequestBody Collect collect) {
        collectService.save(collect);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("收藏删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "收藏ID", required = true) @RequestParam Integer id) {
        Collect collect = new Collect();
        collect.setId(id);
        collect.setStatus(0);
        collectService.update(collect);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("收藏修改")
    @PostMapping("/update")
    public Result update(@RequestBody Collect collect) {
        collectService.update(collect);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("收藏详情")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "收藏ID", required = true) @RequestParam Integer id) {
        Collect collect = collectService.findById(id);
        return ResultGenerator.genSuccessResult(collect);
    }

    @ApiOperation("收藏分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Collect> list = collectService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
