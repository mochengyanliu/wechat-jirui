package com.company.project.controller;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Category;
import com.company.project.service.CategoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2023/03/02.
*/
@Api(tags = "类目模块")
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private CategoryService categoryService;

    @ApiOperation("类目新增")
    @PostMapping("/add")
    public Result add(@RequestBody Category category) {
        categoryService.save(category);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("类目删除")
    @GetMapping("/delete")
    public Result delete(@ApiParam(value = "类目ID", required = true) @RequestParam Integer id) {
        Category category = new Category();
        category.setId(id);
        category.setStatus(0);
        categoryService.update(category);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("类目修改")
    @PostMapping("/update")
    public Result update(@ApiParam(value = "类目ID", required = true) @RequestBody Category category) {
        categoryService.update(category);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("类目详情")
    @GetMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        Category category = categoryService.findById(id);
        return ResultGenerator.genSuccessResult(category);
    }

    @ApiOperation("类目分页")
    @GetMapping("/page")
    public Result page(@ApiParam(value = "页码", required = true) @RequestParam(defaultValue = "0") Integer page,
                       @ApiParam(value = "大小", required = true) @RequestParam(defaultValue = "0") Integer size,
                       @ApiParam(value = "类目名称", required = true) @RequestParam("categoryName") String categoryName) {
        PageHelper.startPage(page, size);
        Condition condition = new Condition(Category.class);
        condition.createCriteria().andEqualTo("status", 1).andLike("categoryName", "%"+categoryName+"%");
        condition.orderBy("createTime").desc();
        List<Category> list = categoryService.findByCondition(condition);
        for (Category category : list) {
            if (StringUtils.isNotEmpty(category.getIconUrl())) {
                category.setIconUrlSrc(picurl + category.getIconUrl());
            }
            if (StringUtils.isNotEmpty(category.getIconUrl())) {
                category.setPicUrlSrc(picurl+ category.getPicUrl());
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @ApiOperation("全部类目")
    @GetMapping("/list")
    public Result list() {
        Condition condition = new Condition(Category.class);
        condition.createCriteria().andEqualTo("status", 1);
        condition.orderBy("sortOrder").asc();
        List<Category> list = categoryService.findByCondition(condition);
        return ResultGenerator.genSuccessResult(list);
    }

}
