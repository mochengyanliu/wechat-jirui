package com.company.project.controller;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Api(tags = "文件模块")
@RestController
@RequestMapping("/file")
public class FileController {

    @Value("${pic.url}")
    private String picurl;

    @PostMapping("upload")
    public Result upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            // 这里是我自定义的异常，可省略
            return ResultGenerator.genFailResult("文件不能为空");
        }
        // 上传文件/图像到指定文件夹（这里可以改成你想存放地址的相对路径）
        File savePos = new File("src/main/resources/static/images");
        if (!savePos.exists()) {  // 不存在，则创建该文件夹
            savePos.mkdir();
        }
        // 获取存放位置的规范路径
        String realPath = savePos.getCanonicalPath();
        // 上传该文件/图像至该文件夹下
        String fileName = UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename();
        file.transferTo(new File(realPath + "/" + fileName));
        Map<String, String> map = new HashMap<>();
        map.put("url", picurl + "/static/images/" + fileName);
        map.put("path", "/static/images/" + fileName);
        return ResultGenerator.genSuccessResult().setData(map);
    }
}
