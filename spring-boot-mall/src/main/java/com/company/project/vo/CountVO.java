package com.company.project.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CountVO {
    private Integer userId;

    private String userName;

    private String mobile;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    private BigDecimal inMoney;

    private BigDecimal outMoney;

    private BigDecimal totalMoney;

    private String shareName;

    private String address;

    private Integer status;

    private String beginTime;

    private String endTime;
}
