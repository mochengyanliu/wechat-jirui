package com.company.project.vo;

import lombok.Data;

@Data
public class SpecificationVo {

    private String specification;

    private String value;

    private String picUrl;
}
