package com.company.project.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.util.List;
import java.util.Map;

@ApiModel("订单接收类")
@Data
public class OrderVo {

    /**
     * 用户主键
     */
    @ApiModelProperty(value = "用户主键")
    private Integer userId;
    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品: [{id:0,num:0,cartId:0},{id:1,num:1,cartId:1}]")
    private List<Map<String, String>> goods;
    /**
     * 收货地址
     */
    @ApiModelProperty(value = "收货地址")
    private Integer addressId;

    /**
     * 订单类型
     */
    @ApiModelProperty(value = "订单类型")
    private Integer type;
}
