package com.company.project.model;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

@ApiModel("订单表")
@Data
@Table(name = "tb_order")
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键",hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	/**
	 * 用户主键
	 */
	@ApiModelProperty(value = "用户主键")
	@Column(name = "user_id")
	private Integer userId;
	/**
	 * 商品id
	 */
	@ApiModelProperty(value = "商品id")
	@Column(name = "goods_id")
	private Integer goodsId;
	/**
	 * 价格
	 */
	@ApiModelProperty(value = "价格")
	private BigDecimal price;
	/**
	 * 数量
	 */
	@ApiModelProperty(value = "数量")
	private Integer num;
	/**
	 * 收货地址
	 */
	@ApiModelProperty(value = "收货地址")
	@Column(name = "address_id")
	private Integer addressId;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@Column(name = "create_time")
	private Date createTime;
	/**
	 * 订单号
	 */
	@ApiModelProperty(value = "订单号")
	@Column(name = "order_no")
	private String orderNo;
	/**
	 * 总价
	 */
	@ApiModelProperty(value = "总价")
	@Column(name = "total_price")
	private BigDecimal totalPrice;
	/**
	 * 物流单号
	 */
	@ApiModelProperty(value = "物流单号")
	@Column(name = "flow_id")
	private String flowId;
	/**
	 * 物流名称
	 */
	@ApiModelProperty(value = "物流名称")
	@Column(name = "flow_type")
	private String flowType;
	/**
	 * 商品编号
	 */
	@ApiModelProperty(value = "商品编号")
	@Column(name = "goods_sn")
	private String goodsSn;
	/**
	 * 商品名称
	 */
	@ApiModelProperty(value = "商品名称")
	@Column(name = "goods_name")
	private String goodsName;
	/**
	 * 商品图片或者商品货品图片
	 */
	@ApiModelProperty(value = "商品图片或者商品货品图片")
	@Column(name = "pic_url")
	private String picUrl;
	/**
	 * 状态：0-删除，1-进行中，2-待发货，3-待收货，4-已完成
	 */
	@ApiModelProperty(value = "状态：0-删除，1-进行中，2-待发货，3-待收货，4-已完成")
	private Integer status;

	@ApiModelProperty(value = "收货手机号")
	private String phone;

	@ApiModelProperty(value = "省份")
	private String province;

	@ApiModelProperty(value = "城市")
	private String city;

	@ApiModelProperty(value = "区域")
	private String district;

	@ApiModelProperty(value = "详细地址")
	private String address;

	@ApiModelProperty(value = "收货人姓名")
	@Column(name = "address_name")
	private String addressName;

	@ApiModelProperty(value = "开始号码")
	@Column(name = "begin_num")
	private Integer beginNum;

	@ApiModelProperty(value = "结束号码")
	@Column(name = "end_num")
	private Integer endNum;

	@ApiModelProperty(value = "随机号码")
	private String random;

	@ApiModelProperty(value = "是否历史：0-否，1-是")
	@Column(name = "is_history")
	private Integer isHistory;

	@ApiModelProperty(value = "中奖号码")
	@Column(name = "win_number")
	private Integer winNumber;

	@ApiModelProperty(value = "中奖时间")
	@Column(name = "win_time")
	private Date winTime;

	@ApiModelProperty(value = "开奖时间")
	@Column(name = "win_date")
	private String winDate;

	@ApiModelProperty(value = "中奖期号")
	@Column(name = "win_issue")
	private String winIssue;

	@ApiModelProperty(value = "中奖代码")
	@Column(name = "win_code")
	private String winCode;

	@ApiModelProperty(value = "参与人数")
	@Column(name = "win_count")
	private Integer winCount;

	@ApiModelProperty(value = "幸运号码")
	@Column(name = "win_luck")
	private Integer winLuck;

	@ApiModelProperty(value = "余数")
	@Column(name = "win_yushu")
	private Integer winYushu;

	@ApiModelProperty(value = "是否中奖：0-否，1-是")
	@Column(name = "is_win")
	private Integer isWin;

	@ApiModelProperty(value = "期号")
	@Column(name = "order_issue")
	private String orderIssue;

	@ApiModelProperty(value = "订单类型：1-渔乐券订单，2-金币订单")
	private Integer type;

	@ApiModelProperty(value = "中奖排序")
	@Column(name = "win_sort")
	private Integer winSort;

	@ApiModelProperty(value = "图片地址")
	@Transient
	private String picUrlSrc;

	@ApiModelProperty(value = "订单用户")
	@Transient
	private String username;

	@ApiModelProperty(value = "用户头像")
	@Transient
	private String image;

	@ApiModelProperty(value = "用户头像路径")
	@Transient
	private String imageSrc;

	@ApiModelProperty(value = "用户手机")
	@Transient
	private String mobile;

	@ApiModelProperty(value = "IP省")
	@Transient
	private String ipProvince;

	@ApiModelProperty(value = "IP市")
	@Transient
	private String ipCity;

	@Transient
	private Integer isRobot;

	@Transient
	private Integer isGold;

	@Transient
	private Integer isBuy;

	@Transient
	private Integer userType;

	@Transient
	private BigDecimal counterPrice;

	@Transient
	private BigDecimal retailPrice;

	@Transient
	private List<String> randomList;

	public List<String> getRandomList() {
		if (random==null){
			return null;
		}
		List<String> list = Arrays.asList(random.split(","));
		Collections.sort(list);
		return list;
	}
}
