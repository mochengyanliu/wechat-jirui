package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@ApiModel("商品基本信息")
@Data
@Table(name = "tb_goods")
public class Goods {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商品编号
     */
    @ApiModelProperty(value = "商品编号")
    @Column(name = "goods_sn")
    private String goodsSn;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 商品所属类目ID
     */
    @ApiModelProperty(value = "商品所属类目ID")
    @Column(name = "category_id")
    private Integer categoryId;

    /**
     * 品牌ID
     */
    @ApiModelProperty(value = "品牌ID")
    @Column(name = "brand_id")
    private Integer brandId;

    /**
     * 商品宣传图片列表，采用JSON数组格式
     */
    @ApiModelProperty(value = "商品宣传图片列表，采用JSON数组格式")
    private String gallery;

    /**
     * 商品关键字，采用逗号间隔
     */
    @ApiModelProperty(value = "商品关键字，采用逗号间隔")
    private String keywords;

    /**
     * 商品简介
     */
    @ApiModelProperty(value = "商品简介")
    private String brief;

    /**
     * 是否上架
     */
    @ApiModelProperty(value = "是否上架")
    @Column(name = "is_on_sale")
    private Integer isOnSale;

    /**
     * 是否可购买：0-否，1-是
     */
    @ApiModelProperty(value = "是否可购买：0-否，1-是")
    @Column(name = "is_buy")
    private Integer isBuy;

    /**
     * 是否是金币兑换：0-否，1-是
     */
    @ApiModelProperty(value = "是否是金币兑换：0-否，1-是")
    @Column(name = "is_gold")
    private Integer isGold;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    @Column(name = "sort_order")
    private Integer sortOrder;

    /**
     * 商品页面商品图片
     */
    @ApiModelProperty(value = "商品页面商品图片")
    @Column(name = "pic_url")
    private String picUrl;

    /**
     * 商品单位，例如件、盒
     */
    @ApiModelProperty(value = "商品单位，例如件、盒")
    private String unit;

    /**
     * 专柜价格
     */
    @ApiModelProperty(value = "专柜价格")
    @Column(name = "counter_price")
    private BigDecimal counterPrice;

    /**
     * 零售价格
     */
    @ApiModelProperty(value = "零售价格")
    @Column(name = "retail_price")
    private BigDecimal retailPrice;

    /**
     * 总价格
     */
    @ApiModelProperty(value = "总价格")
    @Column(name = "total_price")
    private BigDecimal totalPrice;

    /**
     * 总用户数
     */
    @ApiModelProperty(value = "总用户数")
    @Column(name = "total_user")
    private Integer totalUser;

    /**
     * 随机数
     */
    @ApiModelProperty(value = "随机数")
    private Integer random;

    /**
     * 随机数总数
     */
    @ApiModelProperty(value = "随机数总数")
    @Column(name = "random_total")
    private Integer randomTotal;

    /**
     * 随机集合数
     */
    @ApiModelProperty(value = "随机集合数")
    @Column(name = "random_list")
    private String randomList;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 商品详细介绍，是富文本格式
     */
    @ApiModelProperty(value = "商品详细介绍，是富文本格式")
    private String detail;

    @ApiModelProperty(hidden = true)
    @Transient
    private String categoryName;

    @ApiModelProperty(hidden = true)
    @Transient
    private String brandName;

    @ApiModelProperty(hidden = true)
    @Transient
    private String picUrlSrc;

    @ApiModelProperty(hidden = true)
    @Transient
    private List<String> gallerys;

    @ApiModelProperty(hidden = true)
    @Transient
    private List<String> galleryUrls;

    @ApiModelProperty(value = "差价")
    @Transient
    private BigDecimal differPrice;

    @ApiModelProperty(value = "换取记录")
    @Transient
    private List<Order> records;

    @ApiModelProperty(value = "历史获得")
    @Transient
    private List<Order> historys;

    @ApiModelProperty(value = "剩余秒数")
    @Transient
    private Integer second;

    @ApiModelProperty(value = "幸运号码")
    @Transient
    private Integer winLuck;

    @ApiModelProperty(value = "获奖时间")
    @Transient
    private Date winTime;

    @ApiModelProperty(value = "用户姓名")
    @Transient
    private String username;

    @ApiModelProperty(value = "用户头像")
    @Transient
    private String image;

    @ApiModelProperty(value = "用户头像路径")
    @Transient
    private String imageUrl;

    @ApiModelProperty(value = "中奖信息")
    @Transient
    private Order order;

    @ApiModelProperty(value = "订单ID")
    @Transient
    private Integer orderId;

    @ApiModelProperty(value = "中奖排序")
    @Transient
    private Integer winSort;
}