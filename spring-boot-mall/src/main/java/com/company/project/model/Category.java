package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("类目信息")
@Data
@Table(name = "tb_category")
public class Category {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 类目名称
     */
    @ApiModelProperty(value = "类目名称")
    @Column(name = "category_name")
    private String categoryName;

    /**
     * 类目关键字，以JSON数组格式
     */
    @ApiModelProperty(value = "类目关键字，以JSON数组格式")
    private String keywords;

    /**
     * 类目介绍
     */
    @ApiModelProperty(value = "类目介绍")
    @Column(name = "category_desc")
    private String categoryDesc;

    /**
     * 父类目ID
     */
    @ApiModelProperty(value = "父类目ID")
    private Integer pid;

    /**
     * 类目图标
     */
    @ApiModelProperty(value = "类目图标")
    @Column(name = "icon_url")
    private String iconUrl;

    /**
     * 类目图片
     */
    @ApiModelProperty(value = "类目图片")
    @Column(name = "pic_url")
    private String picUrl;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    @Column(name = "sort_order")
    private Integer sortOrder;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;


    @ApiModelProperty(hidden = true)
    @Transient
    private String iconUrlSrc;

    @ApiModelProperty(hidden = true)
    @Transient
    private String picUrlSrc;
}