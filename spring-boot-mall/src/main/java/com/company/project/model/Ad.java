package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("广告信息")
@Data
@Table(name = "tb_ad")
public class Ad {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 广告标题
     */
    @ApiModelProperty(value = "广告标题")
    private String title;

    /**
     * 所广告的商品页面或者活动页面链接地址
     */
    @ApiModelProperty(value = "所广告的商品页面或者活动页面链接地址")
    private String link;

    /**
     * 广告宣传图片
     */
    @ApiModelProperty(value = "广告宣传图片")
    private String url;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    @Column(name = "sort_order")
    private Integer sortOrder;

    /**
     * 广告位置：1则是首页
     */
    @ApiModelProperty(value = "广告位置：1则是首页")
    private Integer position;

    /**
     * 活动内容
     */
    @ApiModelProperty(value = "活动内容")
    private String content;

    /**
     * 广告开始时间
     */
    @ApiModelProperty(value = "广告开始时间")
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 广告结束时间
     */
    @ApiModelProperty(value = "广告结束时间")
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 状态：0-删除，1-启用，2-关闭
     */
    @ApiModelProperty(value = "状态：0-删除，1-启用，2-关闭")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(hidden = true)
    @Transient
    private String urlSrc;
}