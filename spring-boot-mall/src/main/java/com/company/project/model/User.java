package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
@ApiModel("用户信息")
@Data
@Table(name = "tb_user")
public class User {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 性别：男、女
     */
    @ApiModelProperty(value = "性别：男、女")
    private String gender;

    /**
     * 类型：1-普通，2-管理员
     */
    @ApiModelProperty(value = "类型：1-普通，2-管理员")
    private Integer type;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    private BigDecimal money;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String image;

    /**
     * 微信登录openid
     */
    @ApiModelProperty(value = "微信登录openid")
    @Column(name = "wx_openid")
    private String wxOpenid;

    /**
     * 微信登录会话
     */
    @ApiModelProperty(value = "微信登录会话")
    private String session;

    /**
     * 省份
     */
    @ApiModelProperty(value = "省份")
    private String province;

    /**
     * 市区
     */
    @ApiModelProperty(value = "市区")
    private String city;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(hidden = true)
    @Transient
    private String imageUrl;

    /**
     * 添加类型
     */
    @ApiModelProperty(value = "添加类型")
    @Transient
    private Integer addType;

    /**
     * 添加余额
     */
    @ApiModelProperty(value = "添加余额")
    @Transient
    private BigDecimal addMoney;

    /**
     * 修改头像
     */
    @ApiModelProperty(value = "修改头像")
    private String updateImage;

    /**
     * 修改用户名
     */
    @ApiModelProperty(value = "修改用户名")
    private String updateUsername;

    /**
     * 检查状态：1-未审核，2-通过，3-拒绝
     */
    @ApiModelProperty(value = "检查状态：1-未审核，2-通过，3-拒绝")
    private Integer checkUpdate;

    /**
     * 分享ID
     */
    @ApiModelProperty(value = "分享ID")
    private Integer shareId;

    /**
     * 金币
     */
    @ApiModelProperty(value = "金币")
    private BigDecimal gold;

    /**
     * 折扣
     */
    @ApiModelProperty(value = "折扣")
    private BigDecimal rate;

    /**
     * 返利
     */
    @ApiModelProperty(value = "返利")
    private BigDecimal fanli;

    /**
     * 是否是机器人：0-否，1-是
     */
    @ApiModelProperty(value = "是否是机器人：0-否，1-是")
    private Integer isRobot;

    @ApiModelProperty(hidden = true)
    @Transient
    private String updateImageUrl;

    @ApiModelProperty(hidden = true)
    @Transient
    private String shareName;

    @ApiModelProperty(hidden = true)
    @Transient
    private String shareMobile;

}