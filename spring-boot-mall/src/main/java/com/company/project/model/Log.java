package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("操作日志信息")
@Data
@Table(name = "tb_log")
public class Log {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 操作分类
     */
    @ApiModelProperty(value = "操作分类")
    @Column(name = "log_type")
    private String logType;

    /**
     * 操作动作
     */
    @ApiModelProperty(value = "操作动作")
    @Column(name = "log_action")
    private String logAction;

    /**
     * 状态：0-操作失败，1-操作成功
     */
    @ApiModelProperty(value = "状态：0-操作失败，1-操作成功")
    @Column(name = "log_status")
    private Integer logStatus;

    /**
     * 操作结果，或者成功消息，或者失败消息
     */
    @ApiModelProperty(value = "操作结果，或者成功消息，或者失败消息")
    private String result;

    /**
     * 补充信息
     */
    @ApiModelProperty(value = "补充信息")
    @Column(name = "log_comment")
    private String logComment;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;
}