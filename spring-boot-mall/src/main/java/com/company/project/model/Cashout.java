package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@ApiModel("收藏信息")
@Data
@Table(name = "tb_cashout")
public class Cashout {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户表的用户ID
     */
    @ApiModelProperty(value = "用户表的用户ID")
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal money;

    /**
     * 支付宝账号
     */
    @ApiModelProperty(value = "支付宝账号")
    private String account;

    /**
     * 支付宝真实姓名
     */
    @ApiModelProperty(value = "支付宝真实姓名")
    private String username;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String comment;

    /**
     * 状态：0-删除，1-未审核，2-审核成功，3-审核失败
     */
    @ApiModelProperty(value = "状态：0-删除，1-未审核，2-审核成功，3-审核失败")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "姓名")
    @Transient
    private String name;

    @ApiModelProperty(value = "手机号")
    @Transient
    private String mobile;

    @ApiModelProperty(value = "用户余额")
    @Transient
    private BigDecimal userMoney;
}