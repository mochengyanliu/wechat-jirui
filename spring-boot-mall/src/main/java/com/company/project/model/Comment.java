package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("评论信息")
@Data
@Table(name = "tb_comment")
public class Comment {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID")
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容")
    private String content;

    /**
     * 用户表的用户ID
     */
    @ApiModelProperty(value = "用户表的用户ID")
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 是否含有图片:0-无，1-有
     */
    @ApiModelProperty(value = "是否含有图片:0-无，1-有")
    @Column(name = "has_picture")
    private Integer hasPicture;

    /**
     * 图片地址列表，采用JSON数组格式
     */
    @ApiModelProperty(value = "图片地址列表，采用JSON数组格式")
    @Column(name = "pic_urls")
    private String picUrls;

    /**
     * 评分， 1-5
     */
    @ApiModelProperty(value = "评分， 1-5")
    private Integer star;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

}