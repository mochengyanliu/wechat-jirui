package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@ApiModel("金额记录")
@Data
@Table(name = "tb_price_log")
public class PriceLog {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 操作名称
     */
    @ApiModelProperty(value = "操作名称")
    private String name;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    @Column(name = "price")
    private BigDecimal price;

    /**
     * 支付金额
     */
    @ApiModelProperty(value = "支付金额")
    @Column(name = "money")
    private BigDecimal money;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 金额
     */
    @ApiModelProperty(value = "状态：1-未完成，2-已完成")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 检查：1-未检查，2-检查1次，3检查2次，4，已检查
     */
    @ApiModelProperty(value = "检查：1-未检查，2-检查1次，3检查2次，4，已检查")
    private Integer checked;

    /**
     * 金额
     */
    @ApiModelProperty(value = "订单类型：1-渔乐券订单，2-金币订单")
    private Integer type;

    /**
     * 金额
     */
    @ApiModelProperty(value = "分享ID")
    private Integer shareId;

    /**
     * 修改前金额
     */
    @ApiModelProperty(value = "修改前金额")
    @Column(name = "befor_price")
    private BigDecimal beforPrice;

    /**
     * 修改后金额
     */
    @ApiModelProperty(value = "修改后金额")
    @Column(name = "after_price")
    private BigDecimal afterPrice;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    @Transient
    private String username;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    @Transient
    private String mobile;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    @Transient
    private String image;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像路径")
    @Transient
    private String imageUrl;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID")
    @Transient
    private Integer goodsId;

    /**
     * 地址ID
     */
    @ApiModelProperty(value = "地址ID")
    @Transient
    private Integer addressId;

    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量")
    @Transient
    private Integer num;

    @Transient
    private String shareName;
}