package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("商品规格信息")
@Data
@Table(name = "tb_specification")
public class Specification {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商品表的商品ID
     */
    @ApiModelProperty(value = "商品表的商品ID")
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 商品规格名称
     */
    @ApiModelProperty(value = "商品规格名称")
    private String specification;

    /**
     * 商品规格值
     */
    @ApiModelProperty(value = "商品规格值")
    private String value;

    /**
     * 商品规格图片
     */
    @ApiModelProperty(value = "商品规格图片")
    @Column(name = "pic_url")
    private String picUrl;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(hidden = true)
    @Transient
    private String picUrlSrc;

}