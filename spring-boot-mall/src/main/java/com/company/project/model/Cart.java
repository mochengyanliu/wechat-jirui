package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@ApiModel("购物车商品信息")
@Data
@Table(name = "tb_cart")
public class Cart {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户表的用户ID
     */
    @ApiModelProperty(value = "用户表的用户ID,新增修改传")
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 商品表的商品ID
     */
    @ApiModelProperty(value = "商品表的商品ID,新增修改传")
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 商品货品的数量
     */
    @ApiModelProperty(value = "商品货品的数量,新增修改传")
    @Column(name = "cart_number")
    private Integer cartNumber;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;


    /**
     * 商品编号
     */
    @ApiModelProperty(value = "商品编号")
    @Transient
    private String goodsSn;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    @Transient
    private String goodsName;

    /**
     * 商品专柜价格
     */
    @ApiModelProperty(value = "商品专柜价格")
    @Transient
    private BigDecimal retailPrice;

    /**
     * 商品总价格
     */
    @ApiModelProperty(value = "商品总价格")
    @Transient
    private BigDecimal totalPrice;

    /**
     * 商品图片或者商品货品图片
     */
    @ApiModelProperty(value = "商品图片或者商品货品图片")
    @Transient
    private String picUrl;

    /**
     * 商品图片或者商品货品图片
     */
    @ApiModelProperty(value = "商品图片或者商品货品图片")
    @Transient
    private String picUrlSrc;

    /**
     * 购物车用户
     */
    @ApiModelProperty(value = "购物车用户")
    @Transient
    private String username;

    @ApiModelProperty(value = "差价")
    @Transient
    private BigDecimal counterPrice;

    @ApiModelProperty(value = "总价")
    @Transient
    private BigDecimal differPrice;
}