package com.company.project.model;

import lombok.Data;

import java.io.Serializable;

@Data
/**
 * (Dict)实体类
 *
 * @author makejava
 * @since 2023-08-21 14:51:58
 */
public class Dict implements Serializable {
    private static final long serialVersionUID = -26656918547338904L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 字典名称
     */
    private String name;
    /**
     * 字典值
     */
    private String value;
    /**
     * 字典编码
     */
    private String code;
    /**
     * 字典类型
     */
    private String type;
    /**
     * 排序
     */
    private String sort;

}

