package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("收藏信息")
@Data
@Table(name = "tb_collect")
public class Collect {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户表的用户ID
     */
    @ApiModelProperty(value = "用户表的用户ID")
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID")
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

}