package com.company.project.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel("品牌商信息")
@Data
@Table(name = "tb_brand")
public class Brand {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 品牌商名称
     */
    @ApiModelProperty(value = "品牌商名称")
    @Column(name = "brand_name")
    private String brandName;

    /**
     * 品牌商简介
     */
    @ApiModelProperty(value = "品牌商简介")
    @Column(name = "brand_desc")
    private String brandDesc;

    /**
     * 品牌商页的品牌商图片
     */
    @ApiModelProperty(value = "品牌商页的品牌商图片")
    @Column(name = "pic_url")
    private String picUrl;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    @Column(name = "sort_order")
    private Integer sortOrder;

    /**
     * 状态：0-删除，1-可用
     */
    @ApiModelProperty(value = "状态：0-删除，1-可用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(hidden = true)
    @Transient
    private String picUrlSrc;
}