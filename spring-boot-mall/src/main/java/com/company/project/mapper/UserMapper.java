package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.User;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface UserMapper extends Mapper<User> {
    void updateMoney(@Param("price")BigDecimal price, @Param("id") Integer id);

    void checkUserInfo(User user);
    void updateGold(@Param("gold")BigDecimal gold, @Param("id") Integer id);

    User getRandUser();

    List<User> page(@Param("username") String username,@Param("mobile")String mobile, @Param("isRobot") Integer isRobot,
                    @Param("shareName") String shareName,@Param("shareMobile") String shareMobile,@Param("type") Integer type);


    /**
     * 查询充值
     * @param userId
     * @param beginTime
     * @param endTime
     * @return
     */
    BigDecimal selectInMoney(@Param("userId") Integer userId,@Param("beginTime") String beginTime,@Param("endTime") String endTime);

    /**
     * 查询提现
     * @param userId
     * @param beginTime
     * @param endTime
     * @return
     */
    BigDecimal selectOutMoney(@Param("userId") Integer userId,@Param("beginTime") String beginTime,@Param("endTime") String endTime);

    /**
     * 查询消费
     * @param userId
     * @param beginTime
     * @param endTime
     * @return
     */
    BigDecimal selectTotalMoney(@Param("userId") Integer userId,@Param("beginTime") String beginTime,@Param("endTime") String endTime);



    int countSize(@Param("username") String username,@Param("mobile")String mobile, @Param("isRobot") Integer isRobot,
                    @Param("shareName") String shareName);

    User getUserByOpenId(String openId);

    User getUserById(Integer id);
}