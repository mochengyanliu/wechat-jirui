package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Cashout;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CashoutMapper extends Mapper<Cashout> {
    List<Cashout> page(@Param("userId") Integer userId, @Param("userName") String userName);
}