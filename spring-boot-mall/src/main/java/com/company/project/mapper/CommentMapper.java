package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Comment;

public interface CommentMapper extends Mapper<Comment> {
}