package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Specification;

public interface SpecificationMapper extends Mapper<Specification> {
}