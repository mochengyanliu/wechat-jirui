package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Collect;

public interface CollectMapper extends Mapper<Collect> {
}