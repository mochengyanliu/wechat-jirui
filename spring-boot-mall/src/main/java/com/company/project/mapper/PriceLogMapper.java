package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.PriceLog;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PriceLogMapper extends Mapper<PriceLog> {
    PriceLog selectByOrderNo(@Param("orderNo") String orderNo);

    List<PriceLog> page(@Param("userId") Integer userId);

    List<PriceLog> pageList(@Param("userId") Integer userId,@Param("username")String username, @Param("shareName")String shareName, @Param("mobile")String mobile);

    List<PriceLog> checkPrice();

    List<PriceLog> getByShareId(Integer shareId);

    PriceLog selectTodayByUserId(Integer userId);

    BigDecimal selectInCount(Integer userId);

    void deleteData();
}