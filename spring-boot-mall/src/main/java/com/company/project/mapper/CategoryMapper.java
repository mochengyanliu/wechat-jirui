package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Category;

public interface CategoryMapper extends Mapper<Category> {
}