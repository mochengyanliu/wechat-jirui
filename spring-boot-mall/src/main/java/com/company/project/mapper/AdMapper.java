package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Ad;

public interface AdMapper extends Mapper<Ad> {
}