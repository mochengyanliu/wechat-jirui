package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper extends Mapper<Address> {
    void updateIsDefaultByUserId(@Param("id") Integer id,@Param("userId") Integer userId);

    List<Address> page(@Param("userId") Integer userId,@Param("name") String name);

    List<Address> selectByUserId(@Param("userId") Integer userId, @Param("isDefault") Integer isDefault);
}