package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Brand;

public interface BrandMapper extends Mapper<Brand> {
}