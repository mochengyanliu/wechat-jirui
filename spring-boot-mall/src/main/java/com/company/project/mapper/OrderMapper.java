package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Order;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface OrderMapper extends Mapper<Order> {

    List<Order> page(@Param("orderNo") String orderNo, @Param("userId") Integer userId, @Param("status") Integer status, @Param("username") String username,
                     @Param("isWin") Integer isWin, @Param("isRobot")Integer isRobot, @Param("mobile")String mobile);

    List<Order> listByGoodsId(@Param("goodsId") Integer goodsId);

    List<Order> historyByGoodsId(@Param("goodsId") Integer goodsId);

    Order selectByNumBetween(@Param("num") Integer num, @Param("goodsId") Integer goodsId);

    void updateByGoodsId(@Param("orderIssue")String orderIssue,@Param("goodsId") Integer goodsId);

    void updateNoWinByGoodsId(@Param("goodsId") Integer goodsId);

    int selectCountByGoodsId(@Param("goodsId") Integer goodsId);

    List<Order> listByOrderIssue(@Param("orderIssue") String orderIssue);

    Order selectById(Integer id);

    int selectWinCount(@Param("goodsId") Integer goodsId);

    BigDecimal selectOutCount(Integer userId);

    void deleteData();
}