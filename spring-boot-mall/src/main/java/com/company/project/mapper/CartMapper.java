package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Cart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartMapper extends Mapper<Cart> {

    List<Cart> page(@Param("userId") Integer userId, @Param("goodsName") String goodsName);

    Cart selectByGoodsId(@Param("userId") Integer userId, @Param("goodsId") Integer goodsId);
}