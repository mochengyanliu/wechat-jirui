package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Footprint;

public interface FootprintMapper extends Mapper<Footprint> {
}