package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Log;

public interface LogMapper extends Mapper<Log> {
}