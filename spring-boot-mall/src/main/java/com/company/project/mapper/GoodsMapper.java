package com.company.project.mapper;

import com.company.project.core.Mapper;
import com.company.project.model.Goods;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GoodsMapper extends Mapper<Goods> {
    List<Goods> selectPage(@Param("name") String name, @Param("categoryId") Integer categoryId, @Param("type") String type,@Param("isOnSale") Integer isOnSale,@Param("isBuy") Integer isBuy,@Param("isGold") Integer isGold);

    Goods selectById(Integer id);

    void udpateTotalPrice(@Param("price")BigDecimal price,@Param("randomTotal")Integer randomTotal,@Param("id") Integer id);

    List<Goods> selectOrderByPrice();
    List<Goods> selectKnowPage(@Param("name") String name, @Param("categoryId") Integer categoryId, @Param("type") String type,@Param("isOnSale") Integer isOnSale,@Param("isBuy") Integer isBuy);

    Goods getRandGoods();
}