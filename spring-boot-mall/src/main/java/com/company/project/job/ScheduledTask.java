package com.company.project.job;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.Common;
import com.aliyun.teautil.models.RuntimeOptions;
import com.company.project.model.*;
import com.company.project.service.*;
import com.company.project.utils.AliPayConfig;
import com.company.project.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Log4j2
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class ScheduledTask {
    @Resource
    private GoodsService goodsService;

    @Resource
    private OrderService orderService;

    @Resource
    private PriceLogService priceLogService;

    @Resource
    private UserService userService;

    @Resource
    private DictService dictService;

    @Resource
    private RedisUtil redisUtil;

    @Value("${alibaba.accesskey.id}")
    private String keyId;

    @Value("${alibaba.accesskey.secret}")
    private String keySecret;

    @Scheduled(cron = "10 0/5 * * * ?")
    public void win() throws InterruptedException {
        List<Goods> goodsList = goodsService.selectOrderByPrice();
        if (goodsList.size() == 0) {
            return;
        }

        int winNum = 0;

        RestTemplate restTemplate = new RestTemplate();
        JSONArray jsonArray = restTemplate.getForObject("http://vip.lkag3.com/K25d9bd3a7f346d/tx5fc-10.json", JSONArray.class);
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        Integer number = Integer.valueOf(jsonObject.getString("code").replaceAll(",", ""));

        boolean flag = true;
        for (Goods goods : goodsList) {
            Integer count = goods.getCounterPrice().divide(goods.getRetailPrice()).intValue();
            Integer num = number % count + 10000001;
            // 中奖者
            Order order = orderService.selectByNumBetween(num, goods.getId());
            if (order.getIsRobot() != 1 && order.getUserType() == 3 && order.getCounterPrice().compareTo(new BigDecimal(1000)) > 0) {
                // 不是机器人并且是会员
                flag = false;
            }
        }
        Dict dict = dictService.selectByCode("isOpen");
        if (dict.getValue().equals("1") && !flag) {
            winNum = checkOrderWin(goodsList);
        }
        for (Goods goods : goodsList) {
            if (winNum != 0) {
                number = winNum;
                String winNums = String.valueOf(winNum);
                List<String> strings = new ArrayList<>();
                for (int i = 0; i < winNums.length(); i++) {
                    strings.add(winNums.substring(i, i + 1));
                }
                jsonObject.put("code", String.join(",", strings));
            }

            Integer num;
            Integer yushu;
            if (number <= goods.getTotalPrice().divide(goods.getRetailPrice()).intValue()) {
                num = number + 10000001;
                yushu = number;
            } else {
                Integer count = goods.getCounterPrice().divide(goods.getRetailPrice()).intValue();
                num = number % count + 10000001;
                yushu = number % count;
            }

            //获取第几个中奖
            Integer winCount = orderService.selectWinCount(goods.getId());

            // 中奖者
            Order order = orderService.selectByNumBetween(num, goods.getId());

            // 拦截中奖
            User user = userService.findById(order.getUserId());
            redisUtil.del("goodsId:" + goods.getId());

            order.setWinNumber(number);
            order.setWinTime(new Date());
            order.setWinDate(jsonObject.getString("opendate"));
            order.setWinIssue(jsonObject.getString("issue"));
            order.setWinCode(jsonObject.getString("code"));
            order.setWinCount(goods.getTotalUser());
            order.setWinYushu(yushu);
            order.setWinLuck(num);
            order.setIsWin(1);
            order.setStatus(2);
            order.setWinSort(winCount + 1);
            orderService.update(order);

            // 更新所有未中奖订单状态
            orderService.updateNoWinByGoodsId(goods.getId());
            // 更新所有订单
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            orderService.updateByGoodsId(uuid, goods.getId());

            // 恢复商品购买数量
            goods.setTotalPrice(new BigDecimal(0));
            goods.setTotalUser(0);
            //重新计算随机值
            int begin = (int) (goods.getCounterPrice().divide(goods.getRetailPrice()).intValue() * 0.7);
            int end = goods.getCounterPrice().divide(goods.getRetailPrice()).intValue();
            int random = (int) (Math.random() * (end - begin));
            goods.setRandom(random + begin);
            goods.setRandomTotal(0);
            List<Integer> list = new ArrayList<>();
            for (int i = 1; i <= goods.getCounterPrice().divide(goods.getRetailPrice()).intValue(); i++) {
                list.add(i);
            }
            Collections.shuffle(list);
            goods.setRandomList(StringUtils.join(list, ","));
            goodsService.update(goods);

            // 不是机器人发送短信
            if (user.getIsRobot() == 0) {
                sendMsg(order.getMobile(), order.getOrderNo());
            }
        }
    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void price() throws AlipayApiException {
        List<PriceLog> priceLogs = priceLogService.checkPrice();
        for (PriceLog priceLog : priceLogs) {
            AlipayClient client = new DefaultAlipayClient(AliPayConfig.gatewayUrl, AliPayConfig.APP_ID1, AliPayConfig.APP_PRIVATE_KEY1, AliPayConfig.FORMAT, AliPayConfig.CHARSET, AliPayConfig.ALIPAY_PUBLIC_KEY1, AliPayConfig.sign_type);
            AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
            AlipayTradeQueryModel model = new AlipayTradeQueryModel();
            model.setOutTradeNo(priceLog.getOrderNo());
            request.setBizModel(model);
            AlipayTradeQueryResponse response = client.execute(request);
            if (response.isSuccess()) {
                JSONObject data = JSONObject.parseObject(response.getBody());
//                System.out.println(data.toJSONString());
                JSONObject query = data.getJSONObject("alipay_trade_query_response");
                if (query.get("trade_status").equals("TRADE_SUCCESS") || query.get("trade_status").equals("TRADE_FINISHED")) {
                    priceLog.setStatus(2);
                    User user = userService.findById(priceLog.getUserId());
                    priceLog.setBeforPrice(user.getMoney());
                    priceLog.setAfterPrice(user.getMoney().add(priceLog.getPrice()));
                    userService.updateMoney(user.getMoney().add(priceLog.getPrice()), user.getId());
                    Order order = orderService.findBy("orderNo", priceLog.getOrderNo());
                    System.out.println("订单状态：" + order.getStatus());
                    if (order.getStatus() == 1) {
                        System.out.println("修改状态：" + order.getStatus());
                        order.setStatus(2);
                        orderService.update(order);
                    }
                }
//                System.out.println("调用成功");
            } else {
                System.out.println("调用失败," + JSONObject.toJSONString(response));
            }

            if (priceLog.getStatus() == 1 && priceLog.getChecked() == 3) {
                // 失败三次第四次还是没有成功就变为支付失败
                Order order = orderService.findBy("orderNo", priceLog.getOrderNo());
                order.setStatus(5);
                orderService.update(order);
            }
            priceLog.setChecked(priceLog.getChecked() + 1);
            priceLogService.update(priceLog);
        }
    }

    @Scheduled(cron = "0/30 * * * * ?")
    private void robotBuy() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour != 1 && hour != 2 && hour != 3 && hour != 4 && hour != 5) {
            try {
                //时间6-24点
                //数量30至90
                User user = userService.getRandUser();
                int time = (int) (Math.random() * 1000 * 30);
                Thread.sleep(time);
                Goods goods = goodsService.getRandGoods();
                if (goods == null) {
                    return;
                }
                int num = 0;
                if (goods.getCounterPrice().intValue() <= 6000) {
                    num = (int) (goods.getCounterPrice().divide(goods.getRetailPrice()).intValue() * 0.2);
                } else {
                    num = (int) (goods.getCounterPrice().divide(goods.getRetailPrice()).intValue() * 0.1);
                }
                num = (int) (Math.random() * num);
                // 不能大于随机数
//                if (goods.getRandom() < (goods.getRandomTotal() + num)) {
//                    num = goods.getRandom() - goods.getRandomTotal();
//                }
                // 不能超卖
                int price = goods.getCounterPrice().compareTo(goods.getTotalPrice().add(goods.getRetailPrice().multiply(BigDecimal.valueOf(num))));
                if (price < 0) {
                    num = goods.getCounterPrice().divide(goods.getRetailPrice()).intValue() - goods.getTotalPrice().divide(goods.getRetailPrice()).intValue();
                }
                if (num <= 0) {
                    return;
                }

                Order order = new Order();
                order.setOrderNo(randomOrderCode());
                order.setUserId(user.getId());
                order.setGoodsId(goods.getId());
                order.setPrice(goods.getRetailPrice());
                order.setNum(num);
                order.setTotalPrice(goods.getRetailPrice().multiply(BigDecimal.valueOf(order.getNum())));
                order.setGoodsSn(goods.getGoodsSn());
                order.setGoodsName(goods.getGoodsName());
                order.setPicUrl(goods.getPicUrl());
                order.setType(1);//类型
                order.setBeginNum(10000001 + goods.getTotalPrice().divide(goods.getRetailPrice()).intValue());
                order.setEndNum(10000000 + goods.getTotalPrice().divide(goods.getRetailPrice()).intValue() + order.getNum());

                String[] strs = goods.getRandomList().split(",");
                List<Integer> list = new ArrayList<>();
                int base = goods.getTotalPrice().divide(goods.getRetailPrice()).intValue();
                for (int i = base; i < base + order.getNum(); i++) {
                    list.add(10000000 + Integer.valueOf(strs[i]));
                }
                order.setRandom(StringUtils.join(list, ","));
                orderService.save(order);
                // 修改商品总价格
                goodsService.udpateTotalPrice(order.getTotalPrice(), num, goods.getId());
                // 生成价格记录
                PriceLog priceLog = new PriceLog();
                priceLog.setPrice(order.getTotalPrice().negate());
                priceLog.setUserId(user.getId());
                priceLog.setName("购买商品");
                priceLog.setType(1);//类型
                priceLog.setChecked(4);
                //时间（精确到毫秒）
                DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
                String localDate = LocalDateTime.now().format(ofPattern);
                //随机数
                String randomNumeric = RandomStringUtils.randomNumeric(4);
                String orderNo = "NO" + localDate + randomNumeric;
                priceLog.setOrderNo(orderNo);
                priceLogService.save(priceLog);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        System.out.println("机器人购买完成");

    }

    @Scheduled(cron = "0 0 1 * * ?")
    private void deleteData() {
        // 删除订单数据
        orderService.deleteData();
        // 删除购买记录
        priceLogService.deleteData();
    }

    private void sendMsg(String mobile, String orderNo) {
        // 请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_ID 和 ALIBABA_CLOUD_ACCESS_KEY_SECRET。
        // 工程代码泄露可能会导致 AccessKey 泄露，并威胁账号下所有资源的安全性。以下代码示例使用环境变量获取 AccessKey 的方式进行调用，仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html
        Config config = new Config()
                // 必填，您的 AccessKey ID
                .setAccessKeyId("LTAI5t9t1gybKGCTgokv5EZY")
                // 必填，您的 AccessKey Secret
                .setAccessKeySecret("zuasuIOIAvXhMXxrTVG9xLkRhkhmID");
        Client client;
        try {
            client = new Client(config);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // Endpoint 请参考 https://api.aliyun.com/product/Dysmsapi
        config.endpoint = "dysmsapi.aliyuncs.com";
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName("极瑞电子商务")
                .setTemplateCode("SMS_462760909")
                .setPhoneNumbers(mobile)
                .setTemplateParam("{\"orderNo\":\"" + orderNo + "\"}");
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            client.sendSmsWithOptions(sendSmsRequest, runtime);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            Common.assertAsString(error.message);
        }

//        RestTemplate restTemplate = new RestTemplate();
//        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
//        params.add("userid", "12647");
//        params.add("account", "渔乐人电子商务");
//        params.add("password", "123456");
//        params.add("mobile", mobile);
//        params.add("content", "【渔乐人电子商务】亲爱的用户，您的订单" + orderNo + "一举夺魁，请及时进入渔乐人科技查看！如有疑问请联系客服！");
//        params.add("sendTime", "");
//        params.add("action", "send");
//        params.add("extno", "");
//        HttpHeaders headers = new HttpHeaders();
//        //设置请求媒体数据类型
//        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        HttpEntity<MultiValueMap<String, Object>> formEntity = new HttpEntity<>(params, headers);
//
//        String result = restTemplate.postForObject("http://47.106.229.82:8888/sms.aspx", formEntity, String.class);
//        System.out.println("短信发送结果：" + result);
    }

    public static String randomOrderCode() {
        SimpleDateFormat dmDate = new SimpleDateFormat("yyyyMMddHHmmss");
        Random r = new Random();
        StringBuilder rs = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            rs.append(r.nextInt(10));
        }
        String randata = rs.toString();
        Date date = new Date();
        String dateran = dmDate.format(date);
        String Xsode = "NO" + dateran + randata;
        if (Xsode.length() < 24) {
            Xsode = Xsode + 0;
        }
        return Xsode;
    }

    private int checkOrderWin(List<Goods> goodsList) {
        Random random = new Random();
        boolean flag = true;
        for (int i = 0; i < 1000; i++) {
            int randomNumber = random.nextInt(99999 - 10000 + 1) + 10000;
            for (Goods goods : goodsList) {
                Integer count = goods.getCounterPrice().divide(goods.getRetailPrice()).intValue();
                Integer num = randomNumber % count + 10000001;
                // 中奖者
                Order order = orderService.selectByNumBetween(num, goods.getId());
                if (order.getIsRobot() != 1 && order.getUserType() == 3 && order.getCounterPrice().compareTo(new BigDecimal(1000)) > 0) {
                    flag = false;
                }
            }
            if (flag) {
                return randomNumber;
            }
        }
        return 0;
    }
}
