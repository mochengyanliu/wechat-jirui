package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.mapper.PriceLogMapper;
import com.company.project.model.PriceLog;
import com.company.project.service.PriceLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class PriceLogServiceImpl extends AbstractService<PriceLog> implements PriceLogService {
    @Resource
    private PriceLogMapper priceLogMapper;

    @Override
    public PriceLog selectByOrderNo(String orderNo) {
        return priceLogMapper.selectByOrderNo(orderNo);
    }

    @Override
    public List<PriceLog> page(Integer userId) {
        return priceLogMapper.page(userId);
    }
    @Override
    public List<PriceLog> pageList(Integer userId,String username, String shareName,String mobile) {
        return priceLogMapper.pageList(userId, username, shareName, mobile);
    }

    @Override
    public List<PriceLog> checkPrice() {
        return priceLogMapper.checkPrice();
    }

    @Override
    public List<PriceLog> getByShareId(Integer shareId) {
        return priceLogMapper.getByShareId(shareId);
    }

    @Override
    public PriceLog selectTodayByUserId(Integer userId) {
        return priceLogMapper.selectTodayByUserId(userId);
    }

    @Override
    public BigDecimal selectInCount(Integer userId) {
        return priceLogMapper.selectInCount(userId);
    }

    @Override
    public void deleteData() {
        priceLogMapper.deleteData();
    }
}
