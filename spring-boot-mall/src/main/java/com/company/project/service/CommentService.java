package com.company.project.service;
import com.company.project.model.Comment;
import com.company.project.core.Service;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface CommentService extends Service<Comment> {

}
