package com.company.project.service;
import com.company.project.model.Category;
import com.company.project.core.Service;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface CategoryService extends Service<Category> {

}
