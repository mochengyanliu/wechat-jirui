package com.company.project.service;

import com.company.project.core.Service;
import com.company.project.model.Cashout;

import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface CashoutService extends Service<Cashout> {

    List<Cashout> page(Integer userId, String userName);
}
