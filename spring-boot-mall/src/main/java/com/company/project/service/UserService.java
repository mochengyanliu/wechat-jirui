package com.company.project.service;
import com.company.project.model.User;
import com.company.project.core.Service;
import com.company.project.vo.CountVO;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface UserService extends Service<User> {

    void updateMoney(BigDecimal price, Integer id);

    void checkUserInfo(User user);

    void updateGold(BigDecimal price, Integer id);

    User getRandUser();

    List<User> page(String username,String mobile, Integer isRobot, String shareName, String shareMobile, Integer type);


    /**
     * 统计查询
     *
     * @return 查询结果
     */
    List<CountVO> countPage(String username,String mobile, String shareName, String beginTime, String endTime);

    int countSize(String username,String mobile, String shareName);

    User getUserByOpenId(String openId);

    User getUserById(Integer id);
}
