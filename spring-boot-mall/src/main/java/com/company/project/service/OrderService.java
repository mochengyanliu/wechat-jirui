package com.company.project.service;

import com.company.project.core.Service;
import com.company.project.model.Order;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface OrderService extends Service<Order> {

    List<Order> page(String orderNo, Integer userId, Integer status,String username, Integer isWin, Integer isRobot,String mobile);

    List<Order> listByGoodsId(Integer goodsId);

    List<Order> historyByGoodsId(Integer goodsId);

    Order selectByNumBetween(Integer num, Integer goodsId);

    void updateByGoodsId(String orderIssue, Integer goodsId);

    void updateNoWinByGoodsId(Integer goodsId);

    int selectCountByGoodsId(Integer goodsId);

    List<Order> listByOrderIssue(String orderIssue);

    Order selectById(Integer id);

    Integer selectWinCount(Integer goodsId);

    BigDecimal selectOutCount(Integer userId);

    void deleteData();
}
