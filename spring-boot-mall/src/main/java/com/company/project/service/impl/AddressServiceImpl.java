package com.company.project.service.impl;

import com.company.project.mapper.AddressMapper;
import com.company.project.model.Address;
import com.company.project.service.AddressService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class AddressServiceImpl extends AbstractService<Address> implements AddressService {
    @Resource
    private AddressMapper addressMapper;

    @Override
    public void updateIsDefaultByUserId(Integer id,Integer userId) {
        addressMapper.updateIsDefaultByUserId(id, userId);
    }

    @Override
    public List<Address> page(Integer userId, String name) {
        return addressMapper.page(userId, name);
    }

    @Override
    public List<Address> selectByUserId(Integer userId, Integer isDefault) {
        return addressMapper.selectByUserId(userId, isDefault);
    }
}
