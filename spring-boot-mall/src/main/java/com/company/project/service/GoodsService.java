package com.company.project.service;
import com.company.project.model.Goods;
import com.company.project.core.Service;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface GoodsService extends Service<Goods> {

    List<Goods> selectPage(String name, Integer categoryId, String type,Integer isOnSale,Integer isBuy, Integer isGold);

    Goods selectById(Integer id);

    void udpateTotalPrice(BigDecimal price, Integer randomTotal, Integer id);

    List<Goods> selectOrderByPrice();

    List<Goods> selectKnowPage(String name, Integer categoryId, String type,Integer isOnSale,Integer isBuy);

    Goods getRandGoods();
}
