package com.company.project.service.impl;

import com.company.project.mapper.UserMapper;
import com.company.project.model.User;
import com.company.project.service.UserService;
import com.company.project.core.AbstractService;
import com.company.project.vo.CountVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class UserServiceImpl extends AbstractService<User> implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public void updateMoney(BigDecimal price, Integer id) {
        userMapper.updateMoney(price, id);
    }

    @Override
    public void checkUserInfo(User user) {
        userMapper.checkUserInfo(user);
    }

    @Override
    public void updateGold(BigDecimal price, Integer id) {
        userMapper.updateGold(price, id);
    }

    @Override
    public User getRandUser() {
        return userMapper.getRandUser();
    }

    @Override
    public List<User> page(String username,String mobile, Integer isRobot, String shareName, String shareMobile, Integer type) {
        return userMapper.page(username,mobile, isRobot, shareName, shareMobile, type);
    }

    @Override
    public User getUserByOpenId(String openId) {
        return userMapper.getUserByOpenId(openId);
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @Override
    public List<CountVO> countPage(String username,String mobile, String shareName, String beginTime, String endTime) {
        List<User> users = userMapper.page(username,mobile, 0, shareName, null, null);
        List<CountVO> countVOS = new ArrayList<>();
        for (User user : users) {
            CountVO data = new CountVO();
            data.setUserId(user.getId());
            data.setUserName(user.getUsername());
            data.setMobile(user.getMobile());
            data.setShareName(user.getShareName());
            data.setAddress(user.getProvince()+user.getCity());
            data.setStatus(user.getStatus());
            data.setCreateTime(user.getCreateTime());
            BigDecimal inMoney = this.userMapper.selectInMoney(user.getId(),beginTime, endTime);
            data.setInMoney(inMoney);
            BigDecimal outMoney = this.userMapper.selectOutMoney(user.getId(),beginTime, endTime);
            data.setOutMoney(outMoney);
            BigDecimal totalMoney = this.userMapper.selectTotalMoney(user.getId(),beginTime, endTime);
            data.setTotalMoney(totalMoney);
            countVOS.add(data);
        }
        return countVOS;
    }

    @Override
    public int countSize(String username, String mobile, String shareName) {
        return userMapper.countSize(username, mobile, 0, shareName);
    }


}
