package com.company.project.service;

import com.company.project.core.Service;
import com.company.project.model.PriceLog;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface PriceLogService extends Service<PriceLog> {

    PriceLog selectByOrderNo(String orderNo);

    List<PriceLog> page(Integer userId);

    List<PriceLog> pageList(Integer userId,String username, String shareName,String mobile);

    List<PriceLog> checkPrice();

    List<PriceLog> getByShareId(Integer shareId);

    PriceLog selectTodayByUserId(Integer userId);

    BigDecimal selectInCount(Integer userId);

    void deleteData();
}
