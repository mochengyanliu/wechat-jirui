package com.company.project.service;
import com.company.project.model.Cart;
import com.company.project.core.Service;

import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface CartService extends Service<Cart> {
    List<Cart> page(Integer userId,String goodsName);

    Cart selectByGoodsId(Integer userId,Integer goodsId);
}
