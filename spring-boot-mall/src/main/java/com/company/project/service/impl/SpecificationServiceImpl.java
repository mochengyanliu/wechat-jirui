package com.company.project.service.impl;

import com.company.project.mapper.SpecificationMapper;
import com.company.project.model.Specification;
import com.company.project.service.SpecificationService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class SpecificationServiceImpl extends AbstractService<Specification> implements SpecificationService {
    @Resource
    private SpecificationMapper specificationMapper;

}
