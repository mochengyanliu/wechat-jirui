package com.company.project.service.impl;

import com.company.project.mapper.DictMapper;
import com.company.project.model.Dict;
import com.company.project.service.DictService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

/**
 * (Dict)表服务实现类
 *
 * @author makejava
 * @since 2023-08-21 14:54:22
 */
@Service("dictService")
public class DictServiceImpl implements DictService {
    @Resource
    private DictMapper dictMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Dict queryById(Integer id) {
        return this.dictMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param dict  筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @Override
    public Page<Dict> queryByPage(Dict dict, PageRequest pageRequest) {
        long total = this.dictMapper.count(dict);
        return new PageImpl<>(this.dictMapper.queryAllByLimit(dict, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param dict 实例对象
     * @return 实例对象
     */
    @Override
    public Dict insert(Dict dict) {
        this.dictMapper.insert(dict);
        return dict;
    }

    /**
     * 修改数据
     *
     * @param dict 实例对象
     * @return 实例对象
     */
    @Override
    public Dict update(Dict dict) {
        this.dictMapper.update(dict);
        return this.queryById(dict.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteByIds(Integer[] ids) {
        return this.dictMapper.deleteById(ids) > 0;
    }

    @Override
    public Dict selectByCode(String code) {
        return this.dictMapper.selectByCode(code);
    }
}
