package com.company.project.service.impl;

import com.company.project.mapper.BrandMapper;
import com.company.project.model.Brand;
import com.company.project.service.BrandService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class BrandServiceImpl extends AbstractService<Brand> implements BrandService {
    @Resource
    private BrandMapper brandMapper;

}
