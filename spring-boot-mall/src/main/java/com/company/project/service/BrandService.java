package com.company.project.service;
import com.company.project.model.Brand;
import com.company.project.core.Service;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface BrandService extends Service<Brand> {

}
