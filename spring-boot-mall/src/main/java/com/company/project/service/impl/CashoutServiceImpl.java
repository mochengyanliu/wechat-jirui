package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.mapper.CashoutMapper;
import com.company.project.model.Cashout;
import com.company.project.service.CashoutService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class CashoutServiceImpl extends AbstractService<Cashout> implements CashoutService {
    @Resource
    private CashoutMapper cashoutMapper;

    @Override
    public List<Cashout> page(Integer userId, String userName) {
        return cashoutMapper.page(userId, userName);
    }
}
