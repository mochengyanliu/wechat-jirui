package com.company.project.service;
import com.company.project.model.Address;
import com.company.project.core.Service;

import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
public interface AddressService extends Service<Address> {

    void updateIsDefaultByUserId(Integer id,Integer userId);

    List<Address> page(Integer userId, String name);

    List<Address> selectByUserId(Integer userId, Integer isDefault);
}
