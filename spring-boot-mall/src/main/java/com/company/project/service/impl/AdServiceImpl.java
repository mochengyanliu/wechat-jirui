package com.company.project.service.impl;

import com.company.project.mapper.AdMapper;
import com.company.project.model.Ad;
import com.company.project.service.AdService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class AdServiceImpl extends AbstractService<Ad> implements AdService {
    @Resource
    private AdMapper adMapper;

}
