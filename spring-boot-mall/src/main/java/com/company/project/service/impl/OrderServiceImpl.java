package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.mapper.OrderMapper;
import com.company.project.model.Order;
import com.company.project.service.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class OrderServiceImpl extends AbstractService<Order> implements OrderService {

    @Value("${pic.url}")
    private String picurl;
    @Resource
    private OrderMapper orderMapper;

    @Override
    public List<Order> page(String orderNo, Integer userId, Integer status,String username, Integer isWin, Integer isRobot,String mobile) {
        return orderMapper.page(orderNo, userId, status, username, isWin, isRobot,mobile);
    }

    @Override
    public List<Order> listByGoodsId(Integer goodsId) {
        List<Order> orders = orderMapper.listByGoodsId(goodsId);
        for (Order order : orders) {
            if (StringUtils.isNotEmpty(order.getImage())){
                order.setImageSrc(picurl + order.getImage());
            }
        }
        return orders;
    }

    @Override
    public List<Order> historyByGoodsId(Integer goodsId) {
        List<Order> orders = orderMapper.historyByGoodsId(goodsId);
        for (Order order : orders) {
            if (StringUtils.isNotEmpty(order.getImage())){
                order.setImageSrc(picurl + order.getImage());
            }
        }
        return orders;
    }

    @Override
    public Order selectByNumBetween(Integer num,Integer goodsId) {
        return orderMapper.selectByNumBetween(num,goodsId);
    }

    @Override
    public void updateByGoodsId(String orderIssue,Integer goodsId) {
        orderMapper.updateByGoodsId(orderIssue, goodsId);
    }

    @Override
    public void updateNoWinByGoodsId(Integer goodsId) {
        orderMapper.updateNoWinByGoodsId(goodsId);
    }

    @Override
    public int selectCountByGoodsId(Integer goodsId) {
        return orderMapper.selectCountByGoodsId(goodsId);
    }

    @Override
    public List<Order> listByOrderIssue(String orderIssue) {
        List<Order> orders = orderMapper.listByOrderIssue(orderIssue);
        for (Order order : orders) {
            if (StringUtils.isNotEmpty(order.getImage())){
                order.setImageSrc(picurl + order.getImage());
            }
        }
        return orders;
    }

    @Override
    public Order selectById(Integer id) {
        Order order = orderMapper.selectById(id);
        if (StringUtils.isNotEmpty(order.getImage())){
            order.setImageSrc(picurl + order.getImage());
        }
        if (StringUtils.isNotEmpty(order.getPicUrl())){
            order.setPicUrlSrc(picurl + order.getPicUrl());
        }
        return order;
    }

    @Override
    public Integer selectWinCount(Integer goodsId) {
        return orderMapper.selectWinCount(goodsId);
    }

    @Override
    public BigDecimal selectOutCount(Integer userId) {
        return orderMapper.selectOutCount(userId);
    }

    @Override
    public void deleteData() {
        orderMapper.deleteData();
    }
}
