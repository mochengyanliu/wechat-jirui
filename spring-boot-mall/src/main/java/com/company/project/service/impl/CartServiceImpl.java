package com.company.project.service.impl;

import com.company.project.mapper.CartMapper;
import com.company.project.model.Cart;
import com.company.project.service.CartService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class CartServiceImpl extends AbstractService<Cart> implements CartService {
    @Resource
    private CartMapper cartMapper;

    @Override
    public List<Cart> page(Integer userId, String goodsName) {
        return cartMapper.page(userId, goodsName);
    }

    @Override
    public Cart selectByGoodsId(Integer userId, Integer goodsId) {
        return cartMapper.selectByGoodsId(userId, goodsId);
    }
}
