package com.company.project.service.impl;

import com.company.project.mapper.GoodsMapper;
import com.company.project.model.Goods;
import com.company.project.model.Order;
import com.company.project.service.GoodsService;
import com.company.project.core.AbstractService;
import com.company.project.service.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created by CodeGenerator on 2023/03/02.
 */
@Service
@Transactional
public class GoodsServiceImpl extends AbstractService<Goods> implements GoodsService {
    @Resource
    private GoodsMapper goodsMapper;

    @Value("${pic.url}")
    private String picurl;

    @Resource
    private OrderService orderService;

    @Override
    public List<Goods> selectPage(String name, Integer categoryId, String type, Integer isOnSale, Integer isBuy, Integer isGold) {
        List<Goods> list = goodsMapper.selectPage(name, categoryId, type, isOnSale, isBuy,isGold);
        for (Goods goods : list) {
            if (StringUtils.isNotEmpty(goods.getPicUrl())) {
                goods.setPicUrlSrc(picurl + goods.getPicUrl());
            }
            if (StringUtils.isNotEmpty(goods.getGallery())) {
                String[] gallery = goods.getGallery().split(",");
                List<String> list1 = new ArrayList<>();
                List<String> list2 = new ArrayList<>();
                for (String s : gallery) {
                    list1.add(s);
                    list2.add(picurl + s);
                }
                goods.setGallerys(list1);
                goods.setGalleryUrls(list2);
            }
            goods.setDetail(goods.getDetail() == null ? "" : goods.getDetail());
        }
        return list;
    }

    @Override
    public Goods selectById(Integer id) {
        return goodsMapper.selectById(id);
    }

    @Override
    public void udpateTotalPrice(BigDecimal price,Integer randomTotal, Integer id) {
        goodsMapper.udpateTotalPrice(price, randomTotal, id);
    }

    @Override
    public List<Goods> selectOrderByPrice() {
        return goodsMapper.selectOrderByPrice();
    }

    @Override
    public List<Goods> selectKnowPage(String name, Integer categoryId, String type, Integer isOnSale, Integer isBuy) {
        List<Goods> list = goodsMapper.selectKnowPage(name, categoryId, type, isOnSale, isBuy);

        // 去重
        Map<Integer, Goods> uniqueUsers = new LinkedHashMap<>();
        for (Goods goods : list) {
            uniqueUsers.put(goods.getId(), goods);
        }
        List<Goods> result = new ArrayList<>(uniqueUsers.values());
        for (Goods goods : result) {
            if (StringUtils.isNotEmpty(goods.getPicUrl())) {
                goods.setPicUrlSrc(picurl + goods.getPicUrl());
            }
            if (StringUtils.isNotEmpty(goods.getImage())) {
                goods.setImageUrl(picurl + goods.getImage());
            }
            if (goods.getWinLuck() == null) {
                Calendar date = Calendar.getInstance();
                int mi = date.get(Calendar.MINUTE);
                int se = date.get(Calendar.SECOND);
                int cu = 5;
                while (mi > cu) {
                    cu += 5;
                }
                goods.setSecond(cu * 60 + 10 - mi * 60 - se);
            }
            if (StringUtils.isNotEmpty(goods.getGallery())) {
                String[] gallery = goods.getGallery().split(",");
                List<String> list1 = new ArrayList<>();
                List<String> list2 = new ArrayList<>();
                for (String s : gallery) {
                    list1.add(s);
                    list2.add(picurl + s);
                }
                goods.setGallerys(list1);
                goods.setGalleryUrls(list2);
            }
            goods.setDetail(goods.getDetail() == null ? "" : goods.getDetail());

            // 历史中奖
            List<Order> historys = orderService.historyByGoodsId(goods.getId());
            goods.setHistorys(historys);
        }
        return list;
    }

    @Override
    public Goods getRandGoods() {
        return goodsMapper.getRandGoods();
    }
}
