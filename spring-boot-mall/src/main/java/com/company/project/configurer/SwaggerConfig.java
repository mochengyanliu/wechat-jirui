package com.company.project.configurer;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * <p>
 *
 * </p>
 *
 * @author lijun
 * @since 2023-04-11
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //作为Springfox框架的主要接口的构建器,提供合理的默认值和方便的配置方法。
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .pathMapping("/api")
                .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
                .apis(RequestHandlerSelectors.basePackage("com.company.project.controller"))
                .build();//select后面得跟build
    }

    //配置文档信息
    private ApiInfo apiInfo() {
        Contact contact = new Contact("老李头", "http://www.baidu.com", "123@123.com");
        return new ApiInfo(
                "小程序项目", // 标题
                "小程序项目接口文档", // 描述
                "v1.0", // 版本
                "http://www.baidu.com", // 组织链接
                contact, // 联系人信息
                "Apach 2.0 许可", // 许可
                "许可链接", // 许可连接
                new ArrayList<>()// 扩展
        );
    }
}
